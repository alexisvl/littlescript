// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "tsupport.h"

// Supporting modules

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

static void _setup_pool(ls_value_t * pool, size_t n);
static void _setup_label_cache(ls_label_cache_t * label_cache);
static int _fetcher(void * arg, uint16_t loc);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------

static ls_value_t _pool[LS_TEST_NPOOL];

// --- PUBLIC FUNCTIONS --------------------------------------------------------

void ls_test_setup(ls_t * ls, char const * text)
{
	_setup_pool(_pool, LS_TEST_NPOOL);
	ls->_callstack = NULL;
	ls->_pool = _pool;
	ls->_pc = 0;
	ls->_label_cache_i = 0;

	ls->fetcher = _fetcher;
	ls->fetcher_arg = (void *) text;
	ls->line_trace_hook = NULL;
	_setup_label_cache(ls->_label_cache);
}

void ls_setup_run(ls_t * ls, char const * text)
{
	ls->fetcher = _fetcher;
	ls->fetcher_arg = (void *) text;
	ls->line_trace_hook = NULL;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static void _setup_pool(ls_value_t * pool, size_t n)
{
	pool[0].prev = NULL;
	pool[0].next = &pool[1];
	pool[n - 1].prev = &pool[n - 2];
	pool[n - 1].next = NULL;
	for (size_t i = 1; i < (n - 1); i++)
	{
		pool[i].prev = &pool[i - 1];
		pool[i].next = &pool[i + 1];
	}
}

static void _setup_label_cache(ls_label_cache_t * label_cache)
{
	for (size_t i = 0; i < LS_LABEL_CACHE_SIZE; i++)
	{
		label_cache[i].pc = LS_ADDR_NULL;
		label_cache[i].num = 0;
	}
}

static int _fetcher(void * arg, uint16_t loc)
{
	char const * text = (char const *) arg;

	// Slightly naughty memoization of pointer bc i can't be arsed to
	// make the API cleaner right now. We just need to implement read-past-
	// end.
	static char const * p_memo = NULL;
	static size_t sz_memo = 0;

	size_t size;
	if (p_memo == text)
		size = sz_memo;
	else
		size = sz_memo = strlen(text);

	if (loc >= size)
		return 0;
	else
		return (unsigned char) text[loc];
}
