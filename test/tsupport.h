// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

#ifndef TSUPPORT_H
#define TSUPPORT_H

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "ls.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>

// --- PUBLIC MACROS -----------------------------------------------------------

/// Set up the internal jump buffer to catch an error. Normally this is done by
/// the execution engine itself and invisible for this user, but tests focusing
/// on internals will need to use this.
///
/// Used as a block:
///
/// 	LS_TEST_CATCH(ls, evar) {
/// 		handle_error(evar);
/// 	}
///
/// @param ls
/// @param evar - name of variable to create in the block
#define LS_TEST_CATCH(ls) if (setjmp((ls)._error_jmp_buf))

/// Execute some code, expecting an error and asserting on it.
///
/// @param ls
/// @param e - error expected
/// @param code - code body to execute
///
/// TODO this memcpys a jmp_buf - that's not valid
#define LS_EXPECT_ERR(ls, e, code) \
	do {	\
		jmp_buf _holdbuf; \
		memcpy(&_holdbuf, &(ls)._error_jmp_buf, sizeof(jmp_buf)); \
		bool got_error = false; \
		if (setjmp((ls)._error_jmp_buf)) { \
			dammit_assert_enum((ls)._error, ==, (e)); \
			got_error = true; \
		} else { \
			code ; \
		} \
		munit_assert_true(got_error); \
		memcpy(&(ls)._error_jmp_buf, &_holdbuf, sizeof(jmp_buf)); \
	} while (0)

/// Assert on an enum. This just throws on casts to shut up the compiler, be
/// careful...
#define dammit_assert_enum(a, op, b) munit_assert_int((int) (a), op, (int) (b))

// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------

/// Number of entries in the test data pool
#define LS_TEST_NPOOL 1000

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Set up a new ls_t with a string as its source. You must also set up the
/// jump buffer, for which you can use LS_TEST_CATCH().
///
/// An internal static pool of LS_TEST_NPOOL will be initialized.
///
/// @param ls
/// @param text - the source text
void ls_test_setup(ls_t * ls, char const * text);

/// Set up the fetcher to return from a string, but don't initialize anything
/// else. Mainly for use with ls_run().
void ls_setup_run(ls_t * ls, char const * text);

#endif // !defined(TSUPPORT_H)
