// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

#include <munit.h>
#include "tsupport.h"
#include "ls_internal.h"
#include "ls_expr.h"
#include "ls_lex.h"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdio.h>

#pragma GCC diagnostic ignored "-Wunused-parameter"

#define DECLTEST(name) static MunitResult name(const MunitParameter params[], \
	void * data)

DECLTEST(_ls_lex)
{
	ls_t ls;
	ls_token_t tok;
        LS_TEST_CATCH(ls)
	{
		munit_errorf("Caught error: %d\n", ls._error);
		return MUNIT_FAIL;
	}

	// Do two-char opers parse right?
	static const char * operstrings[] = {
		"< <= = <> >= >",
		"<<==<>>=>",
		NULL
	};
	for (size_t i = 0; operstrings[i]; i++)
	{
		ls_test_setup(&ls, operstrings[i]);
		tok = ls_lex(&ls);
		dammit_assert_enum(tok, ==, LS_OP_LT);
		tok = ls_lex(&ls);
		dammit_assert_enum(tok, ==, LS_OP_LEQ);
		tok = ls_lex(&ls);
		dammit_assert_enum(tok, ==, LS_OP_EQ);
		tok = ls_lex(&ls);
		dammit_assert_enum(tok, ==, LS_OP_NEQ);
		tok = ls_lex(&ls);
		dammit_assert_enum(tok, ==, LS_OP_GEQ);
		tok = ls_lex(&ls);
		dammit_assert_enum(tok, ==, LS_OP_GT);
	}

	// Numbers are done separately in _ls_parse_num

	ls_test_setup(&ls, "foo");
	tok = ls_lex(&ls);
	dammit_assert_enum(tok, ==, LS_TOK_WORD);
	munit_assert_string_equal(ls._token.word, "foo");

	ls_test_setup(&ls, "foo bar");
	tok = ls_lex(&ls);
	dammit_assert_enum(tok, ==, LS_TOK_WORD);
	munit_assert_string_equal(ls._token.word, "foo");
	tok = ls_lex(&ls);
	dammit_assert_enum(tok, ==, LS_TOK_WORD);
	munit_assert_string_equal(ls._token.word, "bar");

	ls_test_setup(&ls, "abittoolong foo");
	tok = ls_lex(&ls);
	dammit_assert_enum(tok, ==, LS_TOK_WORD);
	munit_assert_string_equal(ls._token.word, "abittoolo");
	tok = ls_lex(&ls);
	dammit_assert_enum(tok, ==, LS_TOK_WORD);
	munit_assert_string_equal(ls._token.word, "foo");

	ls_test_setup(&ls, "  hasspace");
	tok = ls_lex(&ls);
	dammit_assert_enum(tok, ==, LS_TOK_WORD);
	munit_assert_string_equal(ls._token.word, "hasspace");
	tok = ls_lex(&ls);
	dammit_assert_enum(tok, ==, LS_TOK_NONE);

	ls_test_setup(&ls, "_foo_");
	tok = ls_lex(&ls);
	dammit_assert_enum(tok, ==, LS_TOK_WORD);
	munit_assert_string_equal(ls._token.word, "_foo_");

	ls_test_setup(&ls, "\x7F");
	LS_EXPECT_ERR(ls, LS_SYNTAX_ERROR, ls_lex(&ls));

	return MUNIT_OK;
}

DECLTEST(_ls_convert_kw)
{
	// Same hash
	dammit_assert_enum(ls_convert_kw("ABS"), ==, LS_KW_ABS);
	dammit_assert_enum(ls_convert_kw("CLOSE"), ==, LS_KW_CLOSE);

	dammit_assert_enum(ls_convert_kw("ERASE"), ==, LS_KW_ERASE);
	dammit_assert_enum(ls_convert_kw("ErAsE"), ==, LS_KW_ERASE);

	dammit_assert_enum(ls_convert_kw("FOO"), ==, LS_NOT_A_KW);

	return MUNIT_OK;
}

DECLTEST(_ls_parse_num)
{
	ls_t ls;
        LS_TEST_CATCH(ls)
	{
		munit_errorf("Caught error %d\n", ls._error);
		return MUNIT_FAIL;
	}

	ls_value_t val;

	ls_test_setup(&ls, "42, &H42, &o42, &42, &b101010");

	ls_eval_expr(&ls, &val, LS_TOK_NONE);
	munit_assert_int(val.body.integer.value, ==, 42);
	ls_lex(&ls); // eat the comma

	ls_eval_expr(&ls, &val, LS_TOK_NONE);
	munit_assert_int(val.body.integer.value, ==, 0x42);
	ls_lex(&ls); // eat the comma

	ls_eval_expr(&ls, &val, LS_TOK_NONE);
	munit_assert_int(val.body.integer.value, ==, 042);
	ls_lex(&ls); // eat the comma

	ls_eval_expr(&ls, &val, LS_TOK_NONE);
	munit_assert_int(val.body.integer.value, ==, 042);
	ls_lex(&ls); // eat the comma

	ls_eval_expr(&ls, &val, LS_TOK_NONE);
	munit_assert_int(val.body.integer.value, ==, 42);
	ls_lex(&ls); // eat the comma

	ls_test_setup(&ls, "0e42");
	LS_EXPECT_ERR(ls, LS_SYNTAX_ERROR,
		ls_eval_expr(&ls, &val, LS_TOK_NONE));
	return MUNIT_OK;
}

static void _check_expr(char const * s, int expected)
{
	char const * testing = NULL;
	ls_t ls;

        LS_TEST_CATCH(ls)
	{
		if (testing)
			munit_errorf("Caught error %d in \"%s\"\n",
				ls._error, testing);
		else
			munit_errorf("Caught error %d\n", ls._error);
	}

	testing = s;
	ls_value_t val;
	ls_test_setup(&ls, s);
	size_t sz_pool = ls_mem_avail(&ls);
	ls_eval_expr(&ls, &val, LS_TOK_NONE);
	dammit_assert_enum(val.ty, ==, LS_TY_INT);
	munit_assert_int(val.body.integer.value, ==, expected);
	munit_assert_size(sz_pool, ==, ls_mem_avail(&ls));
}

DECLTEST(_ls_eval_expr)
{
	_check_expr("42", 42);
	_check_expr("(&H28 + (1 + 1))", 42);
	_check_expr("2 + 3 * 4", 14);
	_check_expr("(2 + 3) * 4", 20);
	_check_expr("abs -42", 42);
	_check_expr("\x80 - 42", 42); // compressed notation for abs -42
	_check_expr("10 + -2", 8);
	_check_expr("-2 + 10", 8);
	_check_expr("-2 + -10", -12);
	_check_expr("43 xor 75", 96);
	_check_expr("not 42", -43);
	_check_expr("43 < 42", 0);
	_check_expr("43 <> 42", 1);
	_check_expr("43 = 42", 0);

	// Same strings with all unnecessary whitespace removed. We test them
	// both ways: with whitespace, to ensure whitespace is always handled
	// correctly; without whitespace, to ensure none of the lexer branches
	// accidentally consume extra chars at the end.
	_check_expr("(&H28+(1+1))", 42);
	_check_expr("2+3*4", 14);
	_check_expr("(2+3)*4", 20);
	_check_expr("abs-42", 42);
	_check_expr("\x80-42", 42); // compressed notation for abs -42
	_check_expr("10+-2", 8);
	_check_expr("-2+10", 8);
	_check_expr("-2+-10", -12);
	_check_expr("43xor 75", 96);
	_check_expr("43\27375", 96); // most compact form
	_check_expr("not 42", -43);
	_check_expr("43<42", 0);
	_check_expr("43<>42", 1);
	_check_expr("43=42", 0);

	return MUNIT_OK;
}

DECLTEST(_ls_lookup_var)
{
	ls_t ls;
        LS_TEST_CATCH(ls)
	{
		munit_errorf("Caught error: %d\n", ls._error);
		return MUNIT_FAIL;
	}

	ls_test_setup(&ls, "2*foo + bar/2");

	// Create the global stack frame, currently there's no function to
	// do that
	ls._callstack = ls_alloc(&ls);
	ls._callstack->ty = LS_TY_SCTX_CALL;
	ls._callstack->prev = NULL;
	ls._callstack->next = NULL;
	ls._callstack->body.sctx_call.pc = LS_ADDR_NULL;
	ls._callstack->body.sctx_call.readptr = LS_ADDR_NULL;

	ls_value_t * var_foo = ls_new_var(&ls, "foo");
	ls_write_int_var(&ls, var_foo, 20);

	ls_value_t * var_bar = ls_new_var(&ls, "bar");
	ls_write_int_var(&ls, var_bar, 4);

	ls_value_t val;
	size_t sz_pool = ls_mem_avail(&ls);
	ls_eval_expr(&ls, &val, LS_TOK_NONE);
	dammit_assert_enum(val.ty, ==, LS_TY_INT);
	munit_assert_int(val.body.integer.value, ==, 42);
	munit_assert_size(sz_pool, ==, ls_mem_avail(&ls));

	return MUNIT_OK;
}

DECLTEST(_ls_let)
{
	ls_t ls;
        LS_TEST_CATCH(ls)
	{
		munit_errorf("Caught error: %d\n", ls._error);
		return MUNIT_FAIL;
	}

	ls_test_setup(&ls,
		"x = 13\n"
		"let y = x * 100\n"
		"z = y + 37\n"
	);

	// Create the global stack frame, currently there's no function to
	// do that
	// TODO can't ls_init() do this now?
	ls._callstack = ls_alloc(&ls);
	ls._callstack->ty = LS_TY_SCTX_CALL;
	ls._callstack->prev = NULL;
	ls._callstack->next = NULL;
	ls._callstack->body.sctx_call.pc = LS_ADDR_NULL;
	ls._callstack->body.sctx_call.readptr = LS_ADDR_NULL;

	ls_exec_line(&ls);
	ls_exec_line(&ls);
	ls_exec_line(&ls);

	ls_value_t * var_z = ls_find_var(&ls, "z");
	munit_assert_int(ls_read_int_var(&ls, var_z), ==, 1337);

	return MUNIT_OK;
}

DECLTEST(_ls_run)
{
	static const size_t szpool = 1000;
	ls_t ls;
	ls_value_t pool[szpool];

	ls_setup_run(&ls,
		"x = 13;let y=x*100\n"
		"z = y + 37\n"
	);

	ls_error_t e = ls_run(&ls, pool, szpool);
	dammit_assert_enum(e, ==, LS_OK);

	ls_value_t * var_z = ls_find_var(&ls, "z");
	munit_assert_int(ls_read_int_var(&ls, var_z), ==, 1337);
/*
	fprintf(stdout, "\n");
	for (size_t i = 0; i < 16; i++)
	{
		ls_print_value(stdout, &pool[i], &pool[0]);
		fprintf(stdout, "\n");
	}
*/
	return MUNIT_OK;
}

DECLTEST(_ls_simple_script)
{
	static const size_t szpool = 1000;
	ls_t ls;
	ls_value_t pool[szpool];

	ls_setup_run(&ls,
		"x = 13\n"
		"y = 24\n"
		"y = y + x\n"
		"x = y + x * 100\n"
		"print \"Hello, world! \",\n"
		"print \"x = \", x, \", twelve = \", 12\n"
	);

	ls_error_t e = ls_run(&ls, pool, szpool);
	dammit_assert_enum(e, ==, LS_OK);

	fprintf(stdout, "\n");
	for (size_t i = 0; i < 16; i++)
	{
		ls_print_value(stdout, &pool[i], &pool[0]);
		fprintf(stdout, "\n");
	}

	return MUNIT_OK;
}

#define TEST(name, func) {name, func, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL}
static MunitTest tests[] = {
	TEST("/ls_lex", _ls_lex),
	TEST("/ls_convert_kw", _ls_convert_kw),
	TEST("/ls_parse_num", _ls_parse_num),
	TEST("/ls_eval_expr", _ls_eval_expr),
	TEST("/ls_lookup_var", _ls_lookup_var),
	TEST("/ls_let", _ls_let),
	TEST("/ls_run", _ls_run),
	TEST("/ls_simple_script", _ls_simple_script),
	{ 0 }
};

static const MunitSuite suite = {
	"/",
	tests,
	NULL, // suites
	1, // iterations
	MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char ** argv)
{
	return munit_suite_main(&suite, NULL, argc, argv);
}
