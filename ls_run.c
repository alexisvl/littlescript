// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "ls.h"
#include "ls_internal.h"
#include "ls_lex.h"

// External dependencies
#include <libexplain/ferror.h>
#include <libexplain/fopen.h>
#include <libexplain/fstat.h>
#include <libexplain/calloc.h>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <limits.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------

typedef struct {
	FILE * f;
	size_t cache_page_size;
	size_t n_cache_pages;
	size_t n_page;
	size_t total_size;

	char ** cache;
	size_t * cache_offsets;
} file_fetcher_t;

// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

static int _fetcher(void * arg, uint16_t loc);
static void _usage(char const * argv0, bool short_text);
static void _tokenize(ls_t * ls);
static void _line_trace_hook(ls_t * ls);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------

static bool cachedebug = false;
static bool pooldebug = false;

// --- PUBLIC FUNCTIONS --------------------------------------------------------

int main(int argc, char ** argv)
{
	enum { ACT_RUN, ACT_TOKENIZE } action = ACT_RUN;
	bool tron = false;

	int opt;
	while ((opt = getopt(argc, argv, "hcpkt")) != -1)
	{
		switch (opt)
		{
		default:
			_usage(argv[0], false);
			exit(opt == 'h' ? EXIT_SUCCESS : EXIT_FAILURE);
			break;

		case 'c':
			cachedebug = true;
			break;

		case 'p':
			pooldebug = true;
			break;

		case 'k':
			action = ACT_TOKENIZE;
			break;

		case 't':
			tron = true;
			break;
		}
	}

	if (optind >= argc)
	{
		_usage(argv[0], true);
		exit(EXIT_FAILURE);
	}

	FILE * f = explain_fopen_or_die(argv[optind], "r");

	struct stat statbuf;
	explain_fstat_or_die(fileno(f), &statbuf);

	size_t const n_cache_pages = 3;
	size_t const cache_page_size = 8; // This should be bigger lol

	file_fetcher_t fet;
	fet.f = f;
	fet.cache_page_size = cache_page_size;
	fet.n_cache_pages = n_cache_pages;
	fet.n_page = (size_t) -1;
	assert(statbuf.st_size >= 0);
	fet.total_size = (size_t) statbuf.st_size;
	fet.cache = explain_calloc_or_die(n_cache_pages, sizeof(*fet.cache));
	fet.cache_offsets = explain_calloc_or_die(n_cache_pages,
		sizeof(*fet.cache_offsets));
	for (size_t i = 0; i < n_cache_pages; i++)
	{
		fet.cache[i] = explain_calloc_or_die(cache_page_size,
			sizeof(char));
		fet.cache_offsets[i] = (size_t) -1;
	}

	ls_t ls;
	ls.fetcher = _fetcher;
	ls.fetcher_arg = &fet;
	ls.line_trace_hook = tron ? _line_trace_hook : NULL;

	if (action == ACT_TOKENIZE)
	{
		ls._pc = 0;
		ls._labels = NULL;
		_tokenize(&ls);
		exit(EXIT_SUCCESS);
	}

	ls_value_t pool[1000];
	ls_error_t e = ls_run(&ls, pool, 1000);

	uint16_t line = 0, col = 0;
	ls_translate_pc(&ls, ls._pc, &line, &col);

	if (e == LS_STOPPED)
	{
		fprintf(stderr, ">stopped at %u:%u\n", line, col);
	}
	else if (e != 0)
	{
		fprintf(stderr, ">error %d at %u:%u\n", (int) e, line, col);
	}

	fclose(f);

	if (pooldebug)
	{
		printf("\n\n==== POOL ====\n");

		printf("First free: <%.4u>\n\n", (unsigned)(ls._pool - pool));
		for (size_t i = 0; i < sizeof(pool)/sizeof(pool[0]); i++)
		{
			ls_print_value(stdout, &pool[i], &pool[0]);
			printf("\n");
			if (pool[i].ty == LS_TY_PRISTINE)
				break;
		}
	}

	return 0;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static int _fetcher(void * arg, uint16_t loc)
{
	// TODO: this needs to be optimized a lot, but I'd like to have a good
	// one to provide as a layer for use by implementers (as this will be
	// a very common sort of use case) so will work on that when I get a
	// chance to do it really well.
	file_fetcher_t * self = (file_fetcher_t *) arg;

	if (loc >= self->total_size)
		return -LS_NO_MORE_PROGRAM;

	// Extremely rudimentary cache fetch. A realistic one would be more
	// heavily optimized for hits.
	if (self->n_page != (size_t) -1 &&
		self->cache_offsets[self->n_page] != (size_t) -1 &&
		self->cache_offsets[self->n_page] <= loc &&
		loc - self->cache_offsets[self->n_page] < self->cache_page_size)
	{
		// HIT
		char c = self->cache[self->n_page][
			loc - self->cache_offsets[self->n_page]
		];

		return (unsigned char) c;
	}
	else
	{
		for (size_t i = 0; i < self->n_cache_pages; i++)
		{
			if (self->cache_offsets[i] != (size_t) -1 &&
				self->cache_offsets[i] <= loc &&
				loc - self->cache_offsets[i] < self->cache_page_size)
			{
				if (cachedebug)
					fprintf(stderr, ">FAR HIT: %"PRIu16"\n", loc);
				self->n_page = i;
				char c = self->cache[self->n_page][
					loc - self->cache_offsets[self->n_page]
				];

				return (unsigned char) c;
			}
		}

		if (cachedebug)
			fprintf(stderr, ">MISS: %"PRIu16"\n", loc);
		self->n_page = (self->n_page + 1) % self->n_cache_pages;
		size_t file_loc = (loc / self->cache_page_size)
			* self->cache_page_size;
		assert(file_loc <= LONG_MAX);
		fseek(self->f, (long) file_loc, SEEK_SET);
		self->cache_offsets[self->n_page] = file_loc;
		size_t n = fread(self->cache[self->n_page],
			self->cache_page_size, 1, self->f);
		if (n < self->cache_page_size)
		{
			if (ferror(self->f))
				return -LS_DEVICE_IO_ERROR;
		}

		char c = self->cache[self->n_page][
			loc - self->cache_offsets[self->n_page]
		];

		return (unsigned char) c;
	}
}

static void _usage(char const * argv0, bool short_text)
{
	fprintf(stderr, "usage: %s [OPTION] SCRIPT\n", argv0);

	if (short_text)
		return;

	fprintf(stderr, "Execute a Littlescript file\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "  -h    display this help text\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "  -c    print cache debug text\n");
	fprintf(stderr, "  -k    print tokenization and quit\n");
	fprintf(stderr, "  -p    print pool usage info\n");
	fprintf(stderr, "  -t    trace executing line numbers\n");
}

static void _tokenize(ls_t * ls)
{
	ls_value_t pool[100]; // for labels
	ls_init(ls, pool, sizeof pool / sizeof pool[0]);

	if (setjmp(ls->_error_jmp_buf))
	{
		uint16_t line = 0, col = 0;
		ls_translate_pc(ls, ls->_pc, &line, &col);
		fprintf(stderr, "error %d at %u:%u",
			(int) ls->_error, line, col);
		exit(EXIT_FAILURE);
	}

	for (;;)
	{
		ls_token_t tok = ls_lex(ls);

		switch (tok)
		{
		case LS_TOK_NUMBER:
			printf("NUMBER %"PRId32"\n", ls->_token.number);
			break;
		case LS_TOK_WORD:
			printf("WORD   %s\n", ls->_token.word);
			break;
		case LS_TOK_STR_LABEL:
			printf("SLABEL %s\n", ls->_token.word);
			break;
		case LS_TOK_NUM_LABEL:
			printf("NLABEL %"PRId32"\n", ls->_token.number);
			break;
		case LS_TOK_STRING:
			printf("STRING %"PRIu16" to %"PRIu16"\n",
				ls->_token.string[0],
				ls->_token.string[1]);
			break;
		case LS_TOK_COMMA:
			printf("COMMA  ,\n");
			break;
		case LS_TOK_STATEMENT_SEP:
			printf("ST SEP ;\n");
			break;
		case LS_TOK_INVALID:
			printf("INVALD\n");
			break;
		case LS_OP_ABS:		printf("OPERAT ABS\n"); break;
		case LS_OP_NOT:		printf("OPERAT NOT\n"); break;
		case LS_OP_AND:		printf("OPERAT AND\n"); break;
		case LS_OP_OR:		printf("OPERAT OR\n"); break;
		case LS_OP_XOR:		printf("OPERAT XOR\n"); break;
		case LS_OP_EQV:		printf("OPERAT EQV\n"); break;
		case LS_OP_IMP:		printf("OPERAT IMP\n"); break;
		case LS_OP_NEQ:		printf("OPERAT <>\n"); break;
		case LS_OP_LEQ:		printf("OPERAT <=\n"); break;
		case LS_OP_GEQ:		printf("OPERAT >=\n"); break;
		case LS_OP_LPAREN:	printf("OPERAT (\n"); break;
		case LS_OP_RPAREN:	printf("OPERAT )\n"); break;
		case LS_OP_POW:		printf("OPERAT ^\n"); break;
		case LS_OP_MUL:		printf("OPERAT *\n"); break;
		case LS_OP_DIV:		printf("OPERAT /\n"); break;
		case LS_OP_MOD:		printf("OPERAT MOD\n"); break;
		case LS_OP_NEG:		printf("OPERAT - (neg)\n"); break;
		case LS_OP_ADD:		printf("OPERAT +\n"); break;
		case LS_OP_SUB:		printf("OPERAT - (sub)\n"); break;
		case LS_OP_EQ:		printf("OPERAT =\n"); break;
		case LS_OP_LT:		printf("OPERAT <\n"); break;
		case LS_OP_GT:		printf("OPERAT >\n"); break;
		case LS_NO_OP:		printf("OPERAT --no oper--\n"); break;
		default:
			if (LS_TOK_KEYWORD(tok))
				printf("KEYWRD %s\n",
					ls_kwmap[tok - LS_KW_OFFSET].name
				);
			else
				printf("BADBAD\n"); // what the fuck?
			break;

		case LS_TOK_NONE:
			printf("END\n");
			return;
		}
	}
}

static void _line_trace_hook(ls_t * ls)
{
	uint16_t line = 0, col = 0;
	bool cachedebug_hold = cachedebug;
	cachedebug = false;
	ls_translate_pc(ls, ls->_pc, &line, &col);
	cachedebug = cachedebug_hold;
	fprintf(stderr, ">line %"PRIu16"\n", line);
}
