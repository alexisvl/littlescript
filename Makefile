# This software disclaims copyright. Do what you want with it. Be gay, do
# crime. Originally written by Alexis Lockwood in 2021. Ⓐ

PYTHON ?= python3
LS_INCLUDE := src
LS_SOURCES := src/ls_internal.c src/ls_kws.c src/ls_expr.c src/ls.c \
	src/ls_lex.c src/ls_goto.c \
	src/ls_kw_impl.c src/ls_kw_impl_PRINT.c src/ls_kw_impl_GOTO.c \
	src/ls_kw_impl_GOSUB_RETURN.c

LS_ARGS := -I${LS_INCLUDE} -Lsrc -lls
TEST_ARGS := -Imunit -Lmunit -lmunit test/tsupport.c
LIBEXPLAIN_ARGS := $(shell pkg-config --libs --cflags libexplain)

TESTS := test/test_internal.test

CFLAGS := \
	-O0 -ggdb \
	-std=c99 -D_DEFAULT_SOURCE \
	-Wall -Wextra -Wpedantic -Wshadow -Wconversion -pedantic \
	${EXTRA_FLAGS}

.PHONY: all clean

all: ls_run ls_minify

clean:
	${RM} -f ${LS_SOURCES:.c=.o}
	${RM} -f ${LS_SOURCES:.c=.d}
	${RM} -f src/libls.a
	${RM} -f munit/libmunit.a munit/munit.o
	${RM} -f ${TESTS}
	${RM} -f ls_run ls_minify

include ${LS_SOURCES:.c=.d}

munit/munit.o: CFLAGS := $(filter-out -Wconversion -Wshadow, ${CFLAGS})

ls_run: ls_run.c src/libls.a
	${CC} $< ${LS_ARGS} ${LIBEXPLAIN_ARGS} -o $@ ${CFLAGS}

ls_minify: ls_minify.c src/libls.a
	${CC} $< ${LS_ARGS} ${LIBEXPLAIN_ARGS} -o $@ ${CFLAGS}

src/ls_kws.c: gen_kws.py
	$(PYTHON) gen_kws.py source > $@

src/ls_kws.h: gen_kws.py
	$(PYTHON) gen_kws.py header > $@

check: ${TESTS}
	for i in ${TESTS}; do $${i}; done

munit/libmunit.a: munit/munit.o
	${AR} rcs $@ $^

src/libls.a: ${LS_SOURCES:.c=.o}
	${AR} rcs $@ $^

%.d: %.c
	@$(CC) -MM $(CFLAGS) $< > $@ && sed -e '/^\S/ s,^,$@ src/,' -i $@

%.test: %.c munit/libmunit.a src/libls.a
	${CC} $< ${LS_ARGS} ${TEST_ARGS} -o $@ ${CFLAGS}
