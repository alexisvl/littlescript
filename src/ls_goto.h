// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

#ifndef LS_GOTO_H
#define LS_GOTO_H

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "ls.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Go to a numbered label.
///
/// @param self
/// @param backward - whether to look backwards for the label (otherwise
///   forwards)
/// @param num - number to go to
void ls_goto_num(ls_t * self, bool backward, uint16_t num);

/// Go to an ident (string) label.
///
/// @param self
/// @param ident - ident to go to
void ls_goto_ident(ls_t * self, char const * ident);

#endif // !defined(LS_GOTO_H)
