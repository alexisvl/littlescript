// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "ls_internal.h"
#include "ls_expr.h"
#include "ls_kws.h"
#include "ls_lex.h"
#include "ls_goto.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void ls_kw_fun_GOTO(ls_t * self)
{
	// Allowed syntax:
	// GOTO ident
	// GOTO +num
	// GOTO -num
	ls_token_t tok = ls_lex(self);

	bool backward = false;

	switch (tok)
	{
	case LS_TOK_WORD:
		ls_goto_ident(self, self->_token.word);
		break;
	case LS_OP_SUB:
		backward = true;
		// fall through
	case LS_OP_ADD:
		tok = ls_lex(self);
		if (tok != LS_TOK_NUMBER)
			ls_throw_err(self, LS_SYNTAX_ERROR);

		if (self->_token.number > UINT16_MAX
			|| self->_token.number < 0)
			ls_throw_err(self, LS_SYNTAX_ERROR);

		ls_goto_num(self, backward, (uint16_t)(self->_token.number));
		break;
	default:
		ls_throw_err(self, LS_SYNTAX_ERROR);
	}
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

