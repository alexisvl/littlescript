// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "ls_lex.h"

// Supporting modules
#include "ls_internal.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------

typedef enum {
	CH_KIND_OPER,
	CH_KIND_DIGIT,
	CH_KIND_AMPER,
	CH_KIND_LETTER,
	CH_KIND_SPACE,
	CH_KIND_STR,
	CH_KIND_SIGIL,
	CH_KIND_REM,
	CH_KIND_COMMA,
	CH_KIND_LINESEP,
	CH_KIND_SEP,
	CH_KIND_LABEL,
	CH_KIND_KW,
	CH_KIND_END,
	CH_KIND_INVALID
} ch_kind_t;

// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

static ch_kind_t _ident_ch_kind(unsigned char ch);
static ls_token_t _lex_oper(ls_t * self, ls_uchar ch[2]);
static ls_token_t _lex_num(ls_t * self, ls_uchar ch[2]);
static ls_token_t _lex_word(ls_t * self, ls_uchar ch[2]);
static ls_token_t _lex_str(ls_t * self, ls_uchar ch[2]);
static ls_token_t _lex_kw(ls_t * self, ls_uchar ch[2]);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

ls_token_t ls_lex(ls_t * self)
{
	unsigned char ch[2];
	ch_kind_t ch_kind;
	bool skip_rem = false;

	do {
		ch[0] = ls_fetch_rel(self, 0);
		ch_kind = _ident_ch_kind(ch[0]);
		self->_pc++;

		if (ch_kind == CH_KIND_REM)
			skip_rem = true;
		else if (ch_kind == CH_KIND_LINESEP || ch_kind == CH_KIND_END)
			skip_rem = false;
	} while (ch_kind == CH_KIND_SPACE || skip_rem);

	ch[1] = ls_fetch_rel(self, 0);

	switch (ch_kind)
	{
	case CH_KIND_OPER:
		return _lex_oper(self, ch);
		break;

	case CH_KIND_DIGIT:
	case CH_KIND_AMPER:
		return _lex_num(self, ch);
		break;

	case CH_KIND_LETTER:
		return _lex_word(self, ch);
		break;

	case CH_KIND_STR:
		return _lex_str(self, ch);
		break;

	case CH_KIND_SIGIL:
		ls_throw_err(self, LS_SYNTAX_ERROR);
		return LS_TOK_INVALID; //unreachable
		break;

	case CH_KIND_COMMA:
		return LS_TOK_COMMA;
		break;

	case CH_KIND_LINESEP:
	case CH_KIND_SEP:
		return LS_TOK_STATEMENT_SEP;
		break;

	case CH_KIND_LABEL:
		// These are actually parsed inside digit/word parsers
		ls_throw_err(self, LS_SYNTAX_ERROR);
		return LS_TOK_INVALID; //unreachable
		break;

	case CH_KIND_KW:
		return _lex_kw(self, ch);
		break;

	case CH_KIND_END:
		return LS_TOK_NONE;
		break;
	case CH_KIND_INVALID:
	default:
		ls_throw_err(self, LS_SYNTAX_ERROR);
		return LS_TOK_INVALID; //unreachable
		break;
	}
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static ch_kind_t _ident_ch_kind(unsigned char ch)
{
	// The LUT represents ASCII values 0x20 to 0x5F. Anything below
	// 0x20 except TAB, CR, LF should not appear in source; anything
	// between 0x60 and 0x7F should be shifted down by 0x20 first;
	// anything above 0x7F is a single-byte keyword.

	static const uint8_t ch_kind_lut[64] = {
		CH_KIND_SPACE,	// space
		CH_KIND_OPER,	// !
		CH_KIND_STR,	// "
		CH_KIND_SIGIL,	// #
		CH_KIND_SIGIL,	// $
		CH_KIND_SIGIL,	// %
		CH_KIND_AMPER,	// &
		CH_KIND_REM,	// '
		CH_KIND_OPER,	// (
		CH_KIND_OPER,	// )
		CH_KIND_OPER,	// *
		CH_KIND_OPER,	// +
		CH_KIND_COMMA,	// ,
		CH_KIND_OPER,	// -
		CH_KIND_DIGIT,	// .
		CH_KIND_OPER,	// /
		CH_KIND_DIGIT,	// 0
		CH_KIND_DIGIT,	// 1
		CH_KIND_DIGIT,	// 2
		CH_KIND_DIGIT,	// 3
		CH_KIND_DIGIT,	// 4
		CH_KIND_DIGIT,	// 5
		CH_KIND_DIGIT,	// 6
		CH_KIND_DIGIT,	// 7
		CH_KIND_DIGIT,	// 8
		CH_KIND_DIGIT,	// 9
		CH_KIND_LABEL,	// :
		CH_KIND_SEP,	// ;
		CH_KIND_OPER,	// <
		CH_KIND_OPER,	// =
		CH_KIND_OPER,	// >
		CH_KIND_OPER,	// ?

		CH_KIND_OPER,	// @ `
		CH_KIND_LETTER,	// A a
		CH_KIND_LETTER,	// B b
		CH_KIND_LETTER,	// C c
		CH_KIND_LETTER,	// D d
		CH_KIND_LETTER,	// E e
		CH_KIND_LETTER,	// F f
		CH_KIND_LETTER,	// G g
		CH_KIND_LETTER,	// H h
		CH_KIND_LETTER,	// I i
		CH_KIND_LETTER,	// J j
		CH_KIND_LETTER,	// K k
		CH_KIND_LETTER,	// L l
		CH_KIND_LETTER,	// M m
		CH_KIND_LETTER,	// N n
		CH_KIND_LETTER,	// O o
		CH_KIND_LETTER,	// P p
		CH_KIND_LETTER,	// Q q
		CH_KIND_LETTER,	// R r
		CH_KIND_LETTER,	// S s
		CH_KIND_LETTER,	// T t
		CH_KIND_LETTER,	// U u
		CH_KIND_LETTER,	// V v
		CH_KIND_LETTER,	// W w
		CH_KIND_LETTER,	// X x
		CH_KIND_LETTER,	// Y y
		CH_KIND_LETTER,	// z z
		CH_KIND_OPER,	// [ {
		CH_KIND_OPER,	// bslash |
		CH_KIND_OPER,	// ] }
		CH_KIND_OPER,	// ^ ~
		CH_KIND_LETTER,	// _ del
	};

	if (ch >= 0x80)
		return CH_KIND_KW;
	else if (ch == 0x7F)
		return CH_KIND_INVALID;
	else if (ch >= 0x60)
		return ch_kind_lut[ch - 0x40];
	else if (ch >= 0x20)
		return ch_kind_lut[ch - 0x20];
	else if (ch == '\t' || ch == '\r')
		return CH_KIND_SPACE;
	else if (ch == '\n')
		return CH_KIND_LINESEP;
	else if (ch == 0)
		return CH_KIND_END;
	else
		return CH_KIND_INVALID;
}

static ls_token_t _lex_oper(ls_t * self, ls_uchar ch[2])
{
	static const unsigned char ops[][2] = {
		[LS_OP_LEQ] = "<=",
		[LS_OP_GEQ] = ">=",
		[LS_OP_NEQ] = "<>",
		[LS_OP_LPAREN] = "(",
		[LS_OP_RPAREN] = ")",
		[LS_OP_MOD] = "%",
		[LS_OP_MUL] = "*",
		[LS_OP_ADD] = "+",
		[LS_OP_SUB] = "-",
		[LS_OP_DIV] = "/",
		[LS_OP_POW] = "^",
		[LS_OP_LT] = "<",
		[LS_OP_EQ] = "=",
		[LS_OP_GT] = ">",
	};

	for (uint8_t i = 0; i < sizeof(ops)/sizeof(ops[0]); i++)
	{
		if (ops[i][1]) {
			if (ch[0] == ops[i][0] && ch[1] == ops[i][1])
			{
				self->_pc++; // consume second char
				return (ls_token_t) i;
				break;
			}
		} else {
			if (ch[0] == ops[i][0])
			{
				return (ls_token_t) i;
				break;
			}
		}
	}

	return LS_TOK_INVALID;
}

static ls_token_t _lex_num(ls_t * self, ls_uchar ch[2])
{
	uint8_t radix = 10;
	ls_token_t tok = LS_TOK_NUMBER;
	ls_int_t val = 0;

	if (ch[0] == '&')
	{
		uint8_t nch = 2;
		radix = 8;
		ch[1] = (ls_uchar) toupper(ch[1]);
		if (ch[1] == 'H')
			radix = 16;
		else if (ch[1] == 'B')
			radix = 2;
		else if (ch[1] == 'O')
			radix = 8;
		else
			nch = 1;

		self->_pc = (ls_addr_t)(self->_pc + nch - 1);
		ch[0] = ls_fetch_rel(self, 0);
		self->_pc++;
	}

	for (;;)
	{
		uint8_t digit = 0;
		ch[0] = (ls_uchar) toupper(ch[0]);

		if (ch[0] >= '0' && ch[0] <= '9')
			digit = (uint8_t)(ch[0] - '0');
		else if (ch[0] >= 'A' && ch[0] <= 'F')
			digit = (uint8_t)(ch[0] - 'A' + 10);
		else if (ch[0] == ':')
		{
			if (val > LS_ADDR_MAX || val < 0)
				ls_throw_err(self, LS_SYNTAX_ERROR);

			self->_label_cache_i++;
			self->_label_cache_i %= LS_LABEL_CACHE_SIZE;
			ls_label_cache_t * lc
				= &self->_label_cache[self->_label_cache_i];
			lc->pc = self->_pc;
			lc->num = (ls_addr_t) val;
			tok = LS_TOK_NUM_LABEL;
			break;
		}
		else
		{
			self->_pc--;
			break;
		}

		ch[0] = ls_fetch_rel(self, 0);
		self->_pc++;

		if (digit >= radix)
			ls_throw_err(self, LS_SYNTAX_ERROR);
		else if (tok != LS_TOK_NUM_LABEL)
			val = val * radix + digit;
	}

	self->_token.number = val;
	return tok;
}

static ls_token_t _lex_word(ls_t * self, ls_uchar ch[2])
{
	ls_token_t tok = LS_TOK_WORD;

	memset(self->_token.word, 0, sizeof(self->_token.word));
	for (size_t i = 0;; i++)
	{
		if (i < sizeof(self->_token.word) - 1 && ch[0] <= CHAR_MAX)
			self->_token.word[i] = (char) ch[0];

		ch[0] = ls_fetch_rel(self, 0);
		ch_kind_t ch_kind = _ident_ch_kind(ch[0]);
		self->_pc++;

		if (ch_kind == CH_KIND_LABEL)
		{
			tok = LS_TOK_STR_LABEL;
			break;
		}
		else if (ch_kind != CH_KIND_LETTER
				&& ch_kind != CH_KIND_DIGIT)
		{
			self->_pc--;
			break;
		}
	}

	ls_kw_t kw = ls_convert_kw(self->_token.word);

	if (kw != LS_NOT_A_KW)
	{
		if (tok == LS_TOK_STR_LABEL)
			ls_throw_err(self, LS_SYNTAX_ERROR);

		return (ls_token_t) kw;
	}
	else if (tok == LS_TOK_STR_LABEL)
	{
		// The label might already exist. If it does, we're either
		// seeing it again or shadowing it.

		ls_value_t * label = NULL;
		for (ls_value_t * i = self->_labels; i; i = i->next)
		{
			if (!strncmp(self->_token.word, i->body.label.ident,
				LS_IDENT_LEN))
			{
				label = i;
				break;
			}
		}

		if (label)
		{
			if (label->body.label.pc != self->_pc)
				ls_throw_err(self, LS_DUPLICATE_DEFINITION);
		}
		else
		{
			label = ls_alloc(self);
			label->ty = LS_TY_LABEL;
			label->next = self->_labels;
			strncpy(label->body.label.ident, self->_token.word,
				LS_IDENT_LEN);
			label->body.label.pc = self->_pc;
			self->_labels = label;
		}
	}

	return tok;
}

static ls_token_t _lex_str(ls_t * self, ls_uchar ch[2])
{
	// TODO: handle escapes (how?)
	self->_token.string[0] = self->_pc;
	for (;;)
	{
		ch[0] = ls_fetch_rel(self, 0);
		if (ch[0] == '"')
		{
			self->_token.string[1] = (ls_addr_t)(self->_pc - 1);
			break;
		}
		if (ch[0] == '\n' || ch[0] == 0)
			ls_throw_err(self, LS_SYNTAX_ERROR);
		self->_pc++;
	}
	self->_pc++;

	return LS_TOK_STRING;
}

static ls_token_t _lex_kw(ls_t * self, ls_uchar ch[2])
{
	if (ch[0] >= LS_KW_OFFSET && ch[0] < LS_MAX_KWS)
		return (ls_token_t) ch[0];
	else
		ls_throw_err(self, LS_SYNTAX_ERROR);
	return LS_TOK_INVALID; //unreachable
}
