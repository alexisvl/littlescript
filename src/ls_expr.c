// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "ls_expr.h"

// Supporting modules
#include "ls_internal.h"
#include "ls_lex.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>

// --- PRIVATE MACROS ----------------------------------------------------------

/// Number of operators that can be stacked at once.
#define OPSTACK_N 16

// --- PRIVATE DATATYPES -------------------------------------------------------

/// Operator definition
typedef struct {
	uint8_t precedence : 6;
	bool unary : 1;
	bool right : 1; // associativity
	/// Function for two ints, or for one int (ignore lhs, for unary)
	ls_int_t (*fun_int_int)(ls_int_t lhs, ls_int_t rhs);
} ls_opdef_t;

typedef struct __packed{
	ls_t * self;
	uint8_t opstack[OPSTACK_N];
	uint8_t opstack_i;
	ls_value_t * outstack;

	uint8_t nest;
	bool allow_unary;
} ls_shuntingyard_t;

// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

/// Return the top operator on the stack without popping. Returns LS_NO_OP if
/// empty.
static ls_op_t _top(ls_shuntingyard_t * yard);
/// Pop the top operator off the stack and return it. Throws LS_INTERNAL_ERROR
/// if empty.
static ls_op_t _pop(ls_shuntingyard_t * yard);
/// Push an operator to the stack, throwing LS_OUT_OF_MEMORY if it's full.
static void _push(ls_shuntingyard_t * yard, ls_op_t op);

/// Handle a LS_TOK_WORD. If a word is found, it is transformed into a NUMBER
/// and should be passed along to _handle_number(). (If it is not found, an
/// error is thrown)
static void _handle_word(ls_shuntingyard_t * yard);

/// Handle a LS_TOK_NUMBER
static void _handle_number(ls_shuntingyard_t * yard);

/// Handle a LS_TOK_KEYWORD(). Some keywords need to be transformed into
/// operators, others halt parsing.
///
/// @return LS_TOK_NONE if parsing should halt, or an operator if parsing
/// should proceed to _handle_operator().
static ls_token_t _handle_keyword(ls_shuntingyard_t * yard, ls_token_t tok);
/// Handle a LS_TOK_OPER()
/// @return true if parsing should halt
static bool _handle_operator(ls_shuntingyard_t * yard, ls_token_t tok);
/// Handle all other tokens
///
/// @return true if parsing should halt
static bool _handle_other(ls_shuntingyard_t * yard, ls_token_t tok);

/// Pop one operator off the operator stack and apply it to the output stack.
static void _pop_oper_and_apply(ls_shuntingyard_t * yard);

static ls_int_t _add_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _sub_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _mul_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _div_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _mod_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _abs_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _neg_int(ls_int_t lhs, ls_int_t rhs);

// Logic
static ls_int_t _not_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _and_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _or_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _xor_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _eqv_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _imp_int_int(ls_int_t lhs, ls_int_t rhs);

// Relational
static ls_int_t _eq_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _neq_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _lt_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _gt_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _leq_int_int(ls_int_t lhs, ls_int_t rhs);
static ls_int_t _geq_int_int(ls_int_t lhs, ls_int_t rhs);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------

static const ls_opdef_t _ops[] = {
	[LS_OP_ABS] = { 13, true, true, &_abs_int },
	[LS_OP_NOT] = { 13, true, true, &_not_int },
	[LS_OP_NEG] = { 13, true, true, &_neg_int },

	[LS_OP_MUL] = { 12, false, false, &_mul_int_int },
	[LS_OP_DIV] = { 12, false, false, &_div_int_int },
	[LS_OP_MOD] = { 12, false, false, &_mod_int_int },

	[LS_OP_ADD] = { 11, false, false, &_add_int_int },
	[LS_OP_SUB] = { 11, false, false, &_sub_int_int },

	[LS_OP_LT] = { 9, false, false, &_lt_int_int },
	[LS_OP_GT] = { 9, false, false, &_gt_int_int },
	[LS_OP_LEQ] = { 9, false, false, &_leq_int_int },
	[LS_OP_GEQ] = { 9, false, false, &_geq_int_int },

	[LS_OP_EQ] = { 8, false, false, &_eq_int_int },
	[LS_OP_NEQ] = { 8, false, false, &_neq_int_int },

	[LS_OP_AND] = { 7, false, false, &_and_int_int },
	[LS_OP_XOR] = { 7, false, false, &_xor_int_int },
	[LS_OP_OR] = { 7, false, false, &_or_int_int },
	[LS_OP_EQV] = { 7, false, false, &_eqv_int_int },
	[LS_OP_IMP] = { 7, false, false, &_imp_int_int },
};

// --- PUBLIC FUNCTIONS --------------------------------------------------------

void ls_eval_expr(ls_t * self, ls_value_t * val, ls_token_t firsttok)
{
	ls_shuntingyard_t yard = {
		.self = self,
		.opstack_i = 0,
		.outstack = NULL,
		.nest = 0,
		.allow_unary = true,
	};

	bool done = false, first = true;

	ls_token_t tok = firsttok;

	while (!done)
	{
		ls_addr_t pc_save = self->_pc;

		if (!first || firsttok == LS_TOK_NONE)
			tok = ls_lex(self);
		first = false;

		switch (tok)
		{
		case LS_TOK_WORD:
			_handle_word(&yard);
			// becomes number
			// fall through

		case LS_TOK_NUMBER:
			_handle_number(&yard);
			break;

		default:
			if (LS_TOK_KEYWORD(tok))
			{
				tok = _handle_keyword(&yard, tok);

				if (tok == LS_TOK_NONE)
				{
					done = true;
					break;
				}
				// becomes operator
			}

			if (LS_TOK_OPER(tok))
				done = _handle_operator(&yard, tok);
			else
				done = _handle_other(&yard, tok);
		}

		if (done) {
			// Don't eat the token that ended the line
			self->_pc = pc_save;
		}
	}

	while (yard.opstack_i)
		_pop_oper_and_apply(&yard);

	if (!yard.outstack)
		ls_throw_err(self, LS_SYNTAX_ERROR);

	*val = *yard.outstack;

	if (yard.outstack->next)
		ls_throw_err(self, LS_SYNTAX_ERROR);

	ls_free(self, yard.outstack);
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static ls_op_t _top(ls_shuntingyard_t * yard)
{
	uint8_t i = yard->opstack_i;
	return i ? yard->opstack[i - 1] : LS_NO_OP;
}

static ls_op_t _pop(ls_shuntingyard_t * yard)
{
	if (yard->opstack_i)
		return yard->opstack[--yard->opstack_i];
	else
		ls_throw_err(yard->self, LS_INTERNAL_ERROR);
	// unreachable
	return LS_NO_OP;
}

static void _push(ls_shuntingyard_t * yard, ls_op_t oper)
{
	if (yard->opstack_i >= OPSTACK_N - 1)
		ls_throw_err(yard->self, LS_OUT_OF_MEMORY);
	else
		yard->opstack[yard->opstack_i++] = (uint8_t) oper;
}

static void _pop_oper_and_apply(ls_shuntingyard_t * yard)
{
	ls_op_t const op = _pop(yard);
	ls_opdef_t const * p_op = &_ops[op];

	if (op == LS_OP_LPAREN)
		return;

	ls_value_t * rhs = yard->outstack;
	if (!rhs)
		ls_throw_err(yard->self, LS_SYNTAX_ERROR);

	ls_value_t * lhs = rhs->next;
	if (!p_op->unary)
	{
		if (!lhs)
			ls_throw_err(yard->self, LS_SYNTAX_ERROR);
		yard->outstack = rhs;
		rhs->next = lhs->next;
	}

	// TODO: types
	rhs->body.integer.value = p_op->fun_int_int(
		lhs ? lhs->body.integer.value : 0,
		rhs->body.integer.value
	);

	if (!p_op->unary)
		ls_free(yard->self, lhs);
}

static void _handle_number(ls_shuntingyard_t * yard)
{
	ls_value_t * v = ls_alloc(yard->self);
	v->ty = LS_TY_INT;
	v->body.integer.value = yard->self->_token.number;
	v->prev = NULL;
	v->next = yard->outstack;
	yard->outstack = v;
	yard->allow_unary = false;
}

static void _handle_word(ls_shuntingyard_t * yard)
{
	ls_value_t * var = ls_find_var(yard->self, yard->self->_token.word);

	if (!var)
		ls_throw_err(yard->self, LS_UNDEFINED_VARIABLE);

	// TODO: types
	yard->self->_token.number = ls_read_int_var(yard->self, var);
}

static ls_token_t _handle_keyword(ls_shuntingyard_t * yard, ls_token_t tok)
{
	(void) yard;

	switch (tok)
	{
	case LS_KW_ABS:	return LS_OP_ABS; break;
	case LS_KW_NOT:	return LS_OP_NOT; break;
	case LS_KW_AND:	return LS_OP_AND; break;
	case LS_KW_XOR:	return LS_OP_XOR; break;
	case LS_KW_OR:	return LS_OP_OR; break;
	case LS_KW_EQV:	return LS_OP_EQV; break;
	case LS_KW_IMP:	return LS_OP_IMP; break;
	default:
		return LS_TOK_NONE;
	}
}

static bool _handle_operator(ls_shuntingyard_t * yard, ls_token_t tok)
{
	if (yard->allow_unary)
	{
		if (tok == LS_OP_SUB)
			tok = LS_OP_NEG;
	}

	if (tok == LS_OP_RPAREN)
	{
		// This is likely an enclosing paren for something else
		// (e.g. function call syntax in GOSUB).
		if (yard->nest == 0)
			return true;

		while (_top(yard) != LS_OP_LPAREN)
			_pop_oper_and_apply(yard);
		_pop(yard);
		--yard->nest;
		yard->allow_unary = false;
	}
	else if (tok == LS_OP_LPAREN)
	{
		_push(yard, tok);
		++yard->nest;
		yard->allow_unary = true;
	}
	else
	{
		ls_opdef_t const * p_op = &_ops[tok];
		uint8_t const prec = p_op->precedence;

		while (_top(yard) != LS_NO_OP)
		{
			ls_opdef_t const *p_top = &_ops[_top(yard)];
			uint8_t const tprec = p_top->precedence;

			bool apply = p_op->right
				? prec <  tprec
				: prec <= tprec;

			if (apply)
				_pop_oper_and_apply(yard);
			else
				break;
		}

		_push(yard, tok);
		yard->allow_unary = true;
	}

	return false;
}

static bool _handle_other(ls_shuntingyard_t * yard, ls_token_t tok)
{
	switch (tok)
	{
	case LS_TOK_COMMA:
		yard->allow_unary = true;
		if (!yard->nest)
			return true;
		break;

	case LS_TOK_STATEMENT_SEP:
	case LS_TOK_NONE:
		if (yard->nest)
			ls_throw_err(yard->self, LS_SYNTAX_ERROR);
		else
			return true;
		break;

	default:
		ls_throw_err(yard->self, LS_SYNTAX_ERROR);
	}

	return false;
}

static ls_int_t _add_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs + rhs;
}

static ls_int_t _sub_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs - rhs;
}

static ls_int_t _mul_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs * rhs;
}

static ls_int_t _div_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs / rhs;
}

static ls_int_t _mod_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs % rhs;
}

static ls_int_t _abs_int(ls_int_t lhs, ls_int_t rhs)
{
	(void) lhs;
	return rhs < 0 ? -rhs : rhs;
}

static ls_int_t _neg_int(ls_int_t lhs, ls_int_t rhs)
{
	(void) lhs;
	return -rhs;
}

static ls_int_t _not_int(ls_int_t lhs, ls_int_t rhs)
{
	(void) lhs;
	return ~rhs;
}

static ls_int_t _and_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs & rhs;
}

static ls_int_t _or_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs | rhs;
}

static ls_int_t _xor_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs ^ rhs;
}

static ls_int_t _eqv_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return ~(lhs ^ rhs);
}

static ls_int_t _imp_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return ~(lhs & ~rhs);
}

static ls_int_t _eq_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs == rhs;
}

static ls_int_t _neq_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs != rhs;
}

static ls_int_t _lt_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs < rhs;
}

static ls_int_t _gt_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs > rhs;
}

static ls_int_t _leq_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs <= rhs;
}

static ls_int_t _geq_int_int(ls_int_t lhs, ls_int_t rhs)
{
	return lhs >= rhs;
}
