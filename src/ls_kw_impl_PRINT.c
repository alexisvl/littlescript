// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "ls_internal.h"
#include "ls_expr.h"
#include "ls_kws.h"
#include "ls_lex.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

static void _print_value(FILE * f, ls_value_t const * value);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void ls_kw_fun_PRINT(ls_t * self)
{
	ls_value_t val;
	bool print_lf = true;
	FILE * file = stdout;

	// TODO; implement TO kw

	for (;;)
	{
		ls_token_t tok = ls_lex(self);

		switch (tok)
		{
		case LS_TOK_STRING:
			for (ls_addr_t i = self->_token.string[0];
			     i <= self->_token.string[1];
			     i++)
			{
				unsigned char c = ls_fetch(self, i);
				fprintf(file, "%c", c);
			}
			break;

		case LS_TOK_COMMA:
			print_lf = false;
			break;

		case LS_TOK_STATEMENT_SEP:
		case LS_TOK_NONE:
			goto done;

		default:
			print_lf = true;
			ls_eval_expr(self, &val, tok);
			_print_value(file, &val);
			break;
		}
	}
done:
	if (print_lf)
		fprintf(file, "\n");
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static void _print_value(FILE * f, ls_value_t const * value)
{
	switch (value->ty)
	{
	case LS_TY_INT:
		fprintf(f, "%"PRId32, value->body.integer.value);
		break;
	case LS_TY_STR:
		// TODO
		fprintf(f, "<str>");
		break;
	case LS_TY_LIST:
		// TODO
		fprintf(f, "<list>");
		break;
	case LS_TY_INT_VAR:
	{
		ls_value_t intval;
		intval.ty = LS_TY_INT;
		intval.body.integer.value = value->body.int_var.value;
		_print_value(f, &intval);
		break;
	}
	case LS_TY_VAR:
		_print_value(f, value->body.var.value);
		break;
	default:
		fprintf(f, "<unknown type>");
		break;
	}
}
