// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "ls_internal.h"
#include "ls_kws.h"
#include "ls_expr.h"
#include "ls_lex.h"
#include "ls_goto.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void ls_kw_fun_GOSUB(ls_t * self)
{
	self->_callstack->body.sctx_call.pc = self->_pc;

	ls_token_t tok = ls_lex(self);

	if (tok != LS_TOK_WORD)
		ls_throw_err(self, LS_SYNTAX_ERROR);

	char ident[LS_IDENT_LEN];
	memcpy(ident, self->_token.word, LS_IDENT_LEN);

	ls_value_t * frame = ls_alloc(self);
	*frame = (ls_value_t) {
		.ty = LS_TY_SCTX_CALL,
		.prev = self->_callstack,
		.next = NULL,
		.body.sctx_call = {
			.pc = self->_pc,
			.readptr = LS_ADDR_NULL,
		},
	};

	self->_callstack = frame;

	tok = ls_lex(self);
	if (tok == LS_OP_LPAREN)
	{
		for (;;)
		{
			tok = ls_lex(self);

			if (tok == LS_TOK_WORD)
			{
				ls_value_t * var = ls_new_var(self,
					self->_token.word);

				tok = ls_lex(self);
				if (tok != LS_OP_EQ)
					ls_throw_err(self, LS_SYNTAX_ERROR);

				// Evaluate the expression in the prev scope
				// We had to create the variable already to
				// avoid losing the buffer content
				self->_callstack = self->_callstack->prev;
				ls_value_t val;
				ls_eval_expr(self, &val, LS_TOK_NONE);
				ls_write_var(self, var, &val);
				self->_callstack = frame;
			}
			else if (tok == LS_TOK_COMMA)
				continue;
			else if (tok == LS_OP_RPAREN)
				break;
			else
				ls_throw_err(self, LS_SYNTAX_ERROR);
		}
		tok = ls_lex(self);
	}

	// AS is parsed by RETURN, just check that the line is actually valid
	if (tok == LS_KW_AS)
	{
		tok = ls_lex(self);
		if (tok != LS_TOK_WORD)
			ls_throw_err(self, LS_SYNTAX_ERROR);

		tok = ls_lex(self);
	}

	if (tok != LS_TOK_STATEMENT_SEP && tok != LS_TOK_NONE)
		ls_throw_err(self, LS_SYNTAX_ERROR);

	ls_goto_ident(self, ident);
}

void ls_kw_fun_RETURN(ls_t * self)
{
	ls_value_t val = {.ty = LS_TY_INT, .body.integer.value = 0};
	ls_token_t tok = ls_lex(self);

	if (!LS_TOK_EOS(tok))
		ls_eval_expr(self, &val, tok);

	// Free the scope
	ls_value_t * frame = self->_callstack->prev;
	ls_addr_t pc = self->_callstack->body.sctx_call.pc;
	ls_free_val(self, self->_callstack);
	self->_callstack = frame;
	self->_pc = pc;

	while (!LS_TOK_EOS(tok))
	{
		if (tok == LS_KW_AS)
		{
			tok = ls_lex(self);
			if (tok != LS_TOK_WORD)
				// This was already checked!
				ls_throw_err(self, LS_INTERNAL_ERROR);
			ls_value_t * var = ls_find_var(self, self->_token.word);
			if (!var)
				var = ls_new_var(self, self->_token.word);
			ls_write_var(self, var, &val);
		}
		tok = ls_lex(self);
	}
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
