// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

#ifndef LS_EXPR_H
#define LS_EXPR_H

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "ls.h"
#include "ls_internal.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// --- PUBLIC MACROS -----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PUBLIC DATATYPES --------------------------------------------------------
// --- PUBLIC CONSTANTS --------------------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

/// Parse and evaluate an expression. Any errors are raised.
///
/// @param self
/// @param val - outparam for the resulting value
/// @param tok - optional first token to be included. LS_TOK_NONE if none. If
///   provided, self->_token must still be valid.
void ls_eval_expr(ls_t * self, ls_value_t * val, ls_token_t firsttok);

#endif // !defined(LS_EXPR_H)
