// This code was auto-generated
#include "ls_kws.h"
#include "ls_internal.h"

const LS_PROGMEM uint8_t ls_kw_hashmap_indices[] = {
    /* 0 */ 0,
    /* 1 */ 2,
    /* 2 */ 3,
    /* 3 */ 5,
    /* 4 */ 9,
    /* 5 */ 12,
    /* 6 */ 15,
    /* 7 */ 17,
    /* 8 */ LS_HASHMAP_NULL_VALUE,
    /* 9 */ 18,
    /* 10 */ 20,
    /* 11 */ 22,
    /* 12 */ 24,
    /* 13 */ 27,
    /* 14 */ 28,
    /* 15 */ 29,
    /* 16 */ 32,
    /* 17 */ 34,
    /* 18 */ 35,
    /* 19 */ 36,
    /* 20 */ 37,
    /* 21 */ 39,
    /* 22 */ 40,
    /* 23 */ 43,
    /* 24 */ 45,
    /* 25 */ 46,
    /* 26 */ 49,
    /* 27 */ 51,
    /* 28 */ 52,
    /* 29 */ 55,
    /* 30 */ 57,
};

const LS_PROGMEM uint8_t ls_kw_hashmap[] = {
    /* 0 */ 21,
    /* 1 */ 0x80 | 43,
    /* 2 */ 0x80 | 35,
    /* 3 */ 29,
    /* 4 */ 0x80 | 53,
    /* 5 */ 5,
    /* 6 */ 50,
    /* 7 */ 52,
    /* 8 */ 0x80 | 55,
    /* 9 */ 41,
    /* 10 */ 42,
    /* 11 */ 0x80 | 45,
    /* 12 */ 10,
    /* 13 */ 23,
    /* 14 */ 0x80 | 28,
    /* 15 */ 25,
    /* 16 */ 0x80 | 33,
    /* 17 */ 0x80 | 20,
    /* 18 */ 11,
    /* 19 */ 0x80 | 39,
    /* 20 */ 18,
    /* 21 */ 0x80 | 46,
    /* 22 */ 27,
    /* 23 */ 0x80 | 58,
    /* 24 */ 13,
    /* 25 */ 16,
    /* 26 */ 0x80 | 54,
    /* 27 */ 0x80 | 38,
    /* 28 */ 0x80 | 56,
    /* 29 */ 14,
    /* 30 */ 24,
    /* 31 */ 0x80 | 51,
    /* 32 */ 17,
    /* 33 */ 0x80 | 26,
    /* 34 */ 0x80 | 32,
    /* 35 */ 0x80 | 36,
    /* 36 */ 0x80 | 1,
    /* 37 */ 2,
    /* 38 */ 0x80 | 19,
    /* 39 */ 0x80 | 4,
    /* 40 */ 0,
    /* 41 */ 9,
    /* 42 */ 0x80 | 47,
    /* 43 */ 3,
    /* 44 */ 0x80 | 15,
    /* 45 */ 0x80 | 7,
    /* 46 */ 22,
    /* 47 */ 57,
    /* 48 */ 0x80 | 59,
    /* 49 */ 12,
    /* 50 */ 0x80 | 30,
    /* 51 */ 0x80 | 49,
    /* 52 */ 6,
    /* 53 */ 40,
    /* 54 */ 0x80 | 48,
    /* 55 */ 8,
    /* 56 */ 0x80 | 34,
    /* 57 */ 0x80 | 44,
};

LS_KW_FUN(ABS);
static const LS_PROGMEM char _kw_name_ABS[] = "ABS";
LS_KW_FUN(AND);
static const LS_PROGMEM char _kw_name_AND[] = "AND";
LS_KW_FUN(AS);
static const LS_PROGMEM char _kw_name_AS[] = "AS";
LS_KW_FUN(ASC);
static const LS_PROGMEM char _kw_name_ASC[] = "ASC";
LS_KW_FUN(AT);
static const LS_PROGMEM char _kw_name_AT[] = "AT";
LS_KW_FUN(ATN);
static const LS_PROGMEM char _kw_name_ATN[] = "ATN";
LS_KW_FUN(CALL);
static const LS_PROGMEM char _kw_name_CALL[] = "CALL";
LS_KW_FUN(CAT);
static const LS_PROGMEM char _kw_name_CAT[] = "CAT";
LS_KW_FUN(CHR);
static const LS_PROGMEM char _kw_name_CHR[] = "CHR";
LS_KW_FUN(CLOSE);
static const LS_PROGMEM char _kw_name_CLOSE[] = "CLOSE";
LS_KW_FUN(COS);
static const LS_PROGMEM char _kw_name_COS[] = "COS";
LS_KW_FUN(COUNT);
static const LS_PROGMEM char _kw_name_COUNT[] = "COUNT";
LS_KW_FUN(DATA);
static const LS_PROGMEM char _kw_name_DATA[] = "DATA";
LS_KW_FUN(DEC);
static const LS_PROGMEM char _kw_name_DEC[] = "DEC";
LS_KW_FUN(DEF);
static const LS_PROGMEM char _kw_name_DEF[] = "DEF";
LS_KW_FUN(END);
static const LS_PROGMEM char _kw_name_END[] = "END";
LS_KW_FUN(EQV);
static const LS_PROGMEM char _kw_name_EQV[] = "EQV";
LS_KW_FUN(ERASE);
static const LS_PROGMEM char _kw_name_ERASE[] = "ERASE";
LS_KW_FUN(ERROR);
static const LS_PROGMEM char _kw_name_ERROR[] = "ERROR";
LS_KW_FUN(FN);
static const LS_PROGMEM char _kw_name_FN[] = "FN";
LS_KW_FUN(FOR);
static const LS_PROGMEM char _kw_name_FOR[] = "FOR";
LS_KW_FUN(GOSUB);
static const LS_PROGMEM char _kw_name_GOSUB[] = "GOSUB";
LS_KW_FUN(GOTO);
static const LS_PROGMEM char _kw_name_GOTO[] = "GOTO";
LS_KW_FUN(HEX);
static const LS_PROGMEM char _kw_name_HEX[] = "HEX";
LS_KW_FUN(IF);
static const LS_PROGMEM char _kw_name_IF[] = "IF";
LS_KW_FUN(IMP);
static const LS_PROGMEM char _kw_name_IMP[] = "IMP";
LS_KW_FUN(INPUT);
static const LS_PROGMEM char _kw_name_INPUT[] = "INPUT";
LS_KW_FUN(LEFT);
static const LS_PROGMEM char _kw_name_LEFT[] = "LEFT";
LS_KW_FUN(LET);
static const LS_PROGMEM char _kw_name_LET[] = "LET";
LS_KW_FUN(LOG);
static const LS_PROGMEM char _kw_name_LOG[] = "LOG";
LS_KW_FUN(MID);
static const LS_PROGMEM char _kw_name_MID[] = "MID";
LS_KW_FUN(NEXT);
static const LS_PROGMEM char _kw_name_NEXT[] = "NEXT";
LS_KW_FUN(NOT);
static const LS_PROGMEM char _kw_name_NOT[] = "NOT";
LS_KW_FUN(OCT);
static const LS_PROGMEM char _kw_name_OCT[] = "OCT";
LS_KW_FUN(ON);
static const LS_PROGMEM char _kw_name_ON[] = "ON";
LS_KW_FUN(OR);
static const LS_PROGMEM char _kw_name_OR[] = "OR";
LS_KW_FUN(OPEN);
static const LS_PROGMEM char _kw_name_OPEN[] = "OPEN";
LS_KW_FUN(PACK);
static const LS_PROGMEM char _kw_name_PACK[] = "PACK";
LS_KW_FUN(PRINT);
static const LS_PROGMEM char _kw_name_PRINT[] = "PRINT";
LS_KW_FUN(RANDOMIZE);
static const LS_PROGMEM char _kw_name_RANDOMIZE[] = "RANDOMIZE";
LS_KW_FUN(READ);
static const LS_PROGMEM char _kw_name_READ[] = "READ";
LS_KW_FUN(REM);
static const LS_PROGMEM char _kw_name_REM[] = "REM";
LS_KW_FUN(RESTORE);
static const LS_PROGMEM char _kw_name_RESTORE[] = "RESTORE";
LS_KW_FUN(RETURN);
static const LS_PROGMEM char _kw_name_RETURN[] = "RETURN";
LS_KW_FUN(RIGHT);
static const LS_PROGMEM char _kw_name_RIGHT[] = "RIGHT";
LS_KW_FUN(RND);
static const LS_PROGMEM char _kw_name_RND[] = "RND";
LS_KW_FUN(SIN);
static const LS_PROGMEM char _kw_name_SIN[] = "SIN";
LS_KW_FUN(SQR);
static const LS_PROGMEM char _kw_name_SQR[] = "SQR";
LS_KW_FUN(STEP);
static const LS_PROGMEM char _kw_name_STEP[] = "STEP";
LS_KW_FUN(SWAP);
static const LS_PROGMEM char _kw_name_SWAP[] = "SWAP";
LS_KW_FUN(TAN);
static const LS_PROGMEM char _kw_name_TAN[] = "TAN";
LS_KW_FUN(THEN);
static const LS_PROGMEM char _kw_name_THEN[] = "THEN";
LS_KW_FUN(TO);
static const LS_PROGMEM char _kw_name_TO[] = "TO";
LS_KW_FUN(UNPACK);
static const LS_PROGMEM char _kw_name_UNPACK[] = "UNPACK";
LS_KW_FUN(UNTIL);
static const LS_PROGMEM char _kw_name_UNTIL[] = "UNTIL";
LS_KW_FUN(VAL);
static const LS_PROGMEM char _kw_name_VAL[] = "VAL";
LS_KW_FUN(WEND);
static const LS_PROGMEM char _kw_name_WEND[] = "WEND";
LS_KW_FUN(WHILE);
static const LS_PROGMEM char _kw_name_WHILE[] = "WHILE";
LS_KW_FUN(WRITE);
static const LS_PROGMEM char _kw_name_WRITE[] = "WRITE";
LS_KW_FUN(XOR);
static const LS_PROGMEM char _kw_name_XOR[] = "XOR";

const LS_PROGMEM ls_kwdef_t ls_kwmap[] = {
    /* 0 */ {_kw_name_ABS, ls_kw_fun_ABS },
    /* 1 */ {_kw_name_AND, ls_kw_fun_AND },
    /* 2 */ {_kw_name_AS, ls_kw_fun_AS },
    /* 3 */ {_kw_name_ASC, ls_kw_fun_ASC },
    /* 4 */ {_kw_name_AT, ls_kw_fun_AT },
    /* 5 */ {_kw_name_ATN, ls_kw_fun_ATN },
    /* 6 */ {_kw_name_CALL, ls_kw_fun_CALL },
    /* 7 */ {_kw_name_CAT, ls_kw_fun_CAT },
    /* 8 */ {_kw_name_CHR, ls_kw_fun_CHR },
    /* 9 */ {_kw_name_CLOSE, ls_kw_fun_CLOSE },
    /* 10 */ {_kw_name_COS, ls_kw_fun_COS },
    /* 11 */ {_kw_name_COUNT, ls_kw_fun_COUNT },
    /* 12 */ {_kw_name_DATA, ls_kw_fun_DATA },
    /* 13 */ {_kw_name_DEC, ls_kw_fun_DEC },
    /* 14 */ {_kw_name_DEF, ls_kw_fun_DEF },
    /* 15 */ {_kw_name_END, ls_kw_fun_END },
    /* 16 */ {_kw_name_EQV, ls_kw_fun_EQV },
    /* 17 */ {_kw_name_ERASE, ls_kw_fun_ERASE },
    /* 18 */ {_kw_name_ERROR, ls_kw_fun_ERROR },
    /* 19 */ {_kw_name_FN, ls_kw_fun_FN },
    /* 20 */ {_kw_name_FOR, ls_kw_fun_FOR },
    /* 21 */ {_kw_name_GOSUB, ls_kw_fun_GOSUB },
    /* 22 */ {_kw_name_GOTO, ls_kw_fun_GOTO },
    /* 23 */ {_kw_name_HEX, ls_kw_fun_HEX },
    /* 24 */ {_kw_name_IF, ls_kw_fun_IF },
    /* 25 */ {_kw_name_IMP, ls_kw_fun_IMP },
    /* 26 */ {_kw_name_INPUT, ls_kw_fun_INPUT },
    /* 27 */ {_kw_name_LEFT, ls_kw_fun_LEFT },
    /* 28 */ {_kw_name_LET, ls_kw_fun_LET },
    /* 29 */ {_kw_name_LOG, ls_kw_fun_LOG },
    /* 30 */ {_kw_name_MID, ls_kw_fun_MID },
    /* 31 */ {_kw_name_NEXT, ls_kw_fun_NEXT },
    /* 32 */ {_kw_name_NOT, ls_kw_fun_NOT },
    /* 33 */ {_kw_name_OCT, ls_kw_fun_OCT },
    /* 34 */ {_kw_name_ON, ls_kw_fun_ON },
    /* 35 */ {_kw_name_OR, ls_kw_fun_OR },
    /* 36 */ {_kw_name_OPEN, ls_kw_fun_OPEN },
    /* 37 */ {_kw_name_PACK, ls_kw_fun_PACK },
    /* 38 */ {_kw_name_PRINT, ls_kw_fun_PRINT },
    /* 39 */ {_kw_name_RANDOMIZE, ls_kw_fun_RANDOMIZE },
    /* 40 */ {_kw_name_READ, ls_kw_fun_READ },
    /* 41 */ {_kw_name_REM, ls_kw_fun_REM },
    /* 42 */ {_kw_name_RESTORE, ls_kw_fun_RESTORE },
    /* 43 */ {_kw_name_RETURN, ls_kw_fun_RETURN },
    /* 44 */ {_kw_name_RIGHT, ls_kw_fun_RIGHT },
    /* 45 */ {_kw_name_RND, ls_kw_fun_RND },
    /* 46 */ {_kw_name_SIN, ls_kw_fun_SIN },
    /* 47 */ {_kw_name_SQR, ls_kw_fun_SQR },
    /* 48 */ {_kw_name_STEP, ls_kw_fun_STEP },
    /* 49 */ {_kw_name_SWAP, ls_kw_fun_SWAP },
    /* 50 */ {_kw_name_TAN, ls_kw_fun_TAN },
    /* 51 */ {_kw_name_THEN, ls_kw_fun_THEN },
    /* 52 */ {_kw_name_TO, ls_kw_fun_TO },
    /* 53 */ {_kw_name_UNPACK, ls_kw_fun_UNPACK },
    /* 54 */ {_kw_name_UNTIL, ls_kw_fun_UNTIL },
    /* 55 */ {_kw_name_VAL, ls_kw_fun_VAL },
    /* 56 */ {_kw_name_WEND, ls_kw_fun_WEND },
    /* 57 */ {_kw_name_WHILE, ls_kw_fun_WHILE },
    /* 58 */ {_kw_name_WRITE, ls_kw_fun_WRITE },
    /* 59 */ {_kw_name_XOR, ls_kw_fun_XOR },
};
