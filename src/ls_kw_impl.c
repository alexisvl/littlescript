// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "ls_internal.h"
#include "ls_kws.h"
#include "ls_expr.h"
#include "ls_lex.h"
#include "ls_goto.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

/// Call for keywords without implementations.
static void _no_impl(ls_t * self);

static void _consume_to_eol(ls_t * self);

void ls_kw_fun_GOTO(ls_t * self);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void ls_kw_fun_ABS(ls_t * self) { _no_impl(self); }
void ls_kw_fun_AND(ls_t * self) { _no_impl(self); }
void ls_kw_fun_AS(ls_t * self) { _no_impl(self); }
void ls_kw_fun_ASC(ls_t * self) { _no_impl(self); }
void ls_kw_fun_AT(ls_t * self) { _no_impl(self); }
void ls_kw_fun_ATN(ls_t * self) { _no_impl(self); }
void ls_kw_fun_CALL(ls_t * self) { _no_impl(self); }
void ls_kw_fun_CAT(ls_t * self) { _no_impl(self); }
void ls_kw_fun_CHR(ls_t * self) { _no_impl(self); }
void ls_kw_fun_CLOSE(ls_t * self) { _no_impl(self); }
void ls_kw_fun_COS(ls_t * self) { _no_impl(self); }
void ls_kw_fun_COUNT(ls_t * self) { _no_impl(self); }
void ls_kw_fun_DATA(ls_t * self) { _no_impl(self); }
void ls_kw_fun_DEC(ls_t * self) { _no_impl(self); }
void ls_kw_fun_DEF(ls_t * self) { _no_impl(self); }

void ls_kw_fun_END(ls_t * self) { ls_throw_err(self, LS_STOPPED); }

void ls_kw_fun_EQV(ls_t * self) { _no_impl(self); }
void ls_kw_fun_ERASE(ls_t * self) { _no_impl(self); }
void ls_kw_fun_ERROR(ls_t * self) { _no_impl(self); }
void ls_kw_fun_FN(ls_t * self) { _no_impl(self); }
void ls_kw_fun_FOR(ls_t * self) { _no_impl(self); }
void ls_kw_fun_HEX(ls_t * self) { _no_impl(self); }

void ls_kw_fun_IF(ls_t * self) {
	// TODO how to support ELSE - the current parsing method doesn't
	// really allow it. Should we bother?
	ls_value_t cond;
	ls_eval_expr(self, &cond, LS_TOK_NONE);

	if (cond.ty != LS_TY_INT)
		ls_throw_err(self, LS_SYNTAX_ERROR);

	if (cond.body.integer.value == 0)
	{
		_consume_to_eol(self);
		return;
	}
	else
	{
		ls_token_t tok = ls_lex(self);

		if (tok == LS_KW_GOTO)
			ls_kw_fun_GOTO(self);
		else if (tok == LS_KW_THEN)
			// TODO (THEN) - need to factor something out of
			// ls_exec_line
			_no_impl(self);
		else
			ls_throw_err(self, LS_SYNTAX_ERROR);
	}
}

void ls_kw_fun_IMP(ls_t * self) { _no_impl(self); }
void ls_kw_fun_INPUT(ls_t * self) { _no_impl(self); }
void ls_kw_fun_LEFT(ls_t * self) { _no_impl(self); }
void ls_kw_fun_LET(ls_t * self) { _no_impl(self); }
void ls_kw_fun_LOG(ls_t * self) { _no_impl(self); }
void ls_kw_fun_MID(ls_t * self) { _no_impl(self); }
void ls_kw_fun_NEXT(ls_t * self) { _no_impl(self); }
void ls_kw_fun_NOT(ls_t * self) { _no_impl(self); }
void ls_kw_fun_OCT(ls_t * self) { _no_impl(self); }
void ls_kw_fun_ON(ls_t * self) { _no_impl(self); }
void ls_kw_fun_OR(ls_t * self) { _no_impl(self); }
void ls_kw_fun_OPEN(ls_t * self) { _no_impl(self); }
void ls_kw_fun_PACK(ls_t * self) { _no_impl(self); }
void ls_kw_fun_RANDOMIZE(ls_t * self) { _no_impl(self); }
void ls_kw_fun_READ(ls_t * self) { _no_impl(self); }

void ls_kw_fun_REM(ls_t * self)
{
	_consume_to_eol(self);
}

void ls_kw_fun_RESTORE(ls_t * self) { _no_impl(self); }
void ls_kw_fun_RIGHT(ls_t * self) { _no_impl(self); }
void ls_kw_fun_RND(ls_t * self) { _no_impl(self); }
void ls_kw_fun_SIN(ls_t * self) { _no_impl(self); }
void ls_kw_fun_SQR(ls_t * self) { _no_impl(self); }
void ls_kw_fun_STEP(ls_t * self) { _no_impl(self); }
void ls_kw_fun_SWAP(ls_t * self) { _no_impl(self); }
void ls_kw_fun_TAN(ls_t * self) { _no_impl(self); }
void ls_kw_fun_THEN(ls_t * self) { _no_impl(self); }
void ls_kw_fun_TO(ls_t * self) { _no_impl(self); }
void ls_kw_fun_UNPACK(ls_t * self) { _no_impl(self); }
void ls_kw_fun_UNTIL(ls_t * self) { _no_impl(self); }
void ls_kw_fun_VAL(ls_t * self) { _no_impl(self); }

void ls_kw_fun_WEND(ls_t * self)
{
	if (self->_callstack->ty != LS_TY_SCTX_WHILE)
		ls_throw_err(self, LS_WHILE_WEND_MISMATCH);

	ls_addr_t pc_wend = self->_pc;
	self->_pc = self->_callstack->body.sctx_while.while_pc;

	ls_value_t cond;
	ls_eval_expr(self, &cond, LS_TOK_NONE);

	if (cond.ty != LS_TY_INT)
		ls_throw_err(self, LS_SYNTAX_ERROR);

	if (cond.body.integer.value == 0)
	{
		ls_value_t * ctx = self->_callstack;
		self->_callstack = ctx->prev;
		ls_free_val(self, ctx);
		self->_pc = pc_wend;
	}
}

void ls_kw_fun_WHILE(ls_t * self)
{
	ls_addr_t pc_cond = self->_pc;

	ls_value_t cond;
	ls_eval_expr(self, &cond, LS_TOK_NONE);

	if (cond.ty != LS_TY_INT)
		ls_throw_err(self, LS_SYNTAX_ERROR);

	if (cond.body.integer.value != 0)
	{
		ls_value_t * ctx = ls_alloc(self);
		*ctx = (ls_value_t) {
			.ty = LS_TY_SCTX_WHILE,
			.body.sctx_while = {
				.while_pc = pc_cond,
			},
			.prev = self->_callstack,
			.next = NULL,
		};
		self->_callstack = ctx;
	}
	else
	{
		bool keyword_allowed = false;
		ls_token_t tok = LS_TOK_NUMBER;
		int8_t nest = 0;

		while (tok != LS_TOK_NONE)
		{
			tok = ls_lex(self);

			if (keyword_allowed)
			{
				if (tok == LS_KW_WHILE)
					nest++;
				else if (tok == LS_KW_WEND)
					if (nest)
						nest--;
					else
						break;
				else if (tok == LS_KW_REM)
				{
					_consume_to_eol(self);
					continue; // keep keyword true
				}
			}

			keyword_allowed =
				tok == LS_TOK_STR_LABEL ||
				tok == LS_TOK_NUM_LABEL ||
				tok == LS_TOK_STATEMENT_SEP;
		}

		if (tok == LS_TOK_NONE)
		{
			self->_pc = pc_cond;
			ls_throw_err(self, LS_WHILE_WEND_MISMATCH);
		}
	}
}

void ls_kw_fun_WRITE(ls_t * self) { _no_impl(self); }
void ls_kw_fun_XOR(ls_t * self) { _no_impl(self); }

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static void _no_impl(ls_t * self)
{
	ls_throw_err(self, LS_BAD_KEYWORD);
}

static void _consume_to_eol(ls_t * self)
{
	for (;;) {
		ls_uchar uch = ls_fetch_rel(self, 0);

		if (uch == 0 || uch == '\n')
			return;

		self->_pc++;
	}
}
