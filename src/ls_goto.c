// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "ls_goto.h"

// Supporting modules
#include "ls_internal.h"
#include "ls_lex.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

static bool _rewind_line(ls_t * self);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void ls_goto_num(ls_t * self, bool backward, uint16_t num)
{
	ls_addr_t target = LS_ADDR_NULL;

	for (size_t i = 0; i < LS_LABEL_CACHE_SIZE; i++)
	{
		if (self->_label_cache[i].pc == LS_ADDR_NULL)
			continue;
		if (self->_label_cache[i].num == num)
		{
			if ((self->_label_cache[i].pc > self->_pc && !backward) ||
				(self->_label_cache[i].pc <= self->_pc && backward))
			{
				target = self->_label_cache[i].pc;
				break;
			}
		}
	}

	// In case we can't find the label, save pc to "unwind" for a
	// more helpful error later
	ls_addr_t pc = self->_pc;

	if (target == LS_ADDR_NULL && !backward)
	{
		// Walk forward until we find the label
		for (;;)
		{
			ls_token_t tok = ls_lex(self);

			if (tok == LS_TOK_NUM_LABEL)
			{
				if (self->_token.number == num)
				{
					target = self->_pc;
					break;
				}
			}
			else if (tok == LS_TOK_NONE)
				break;
		}
	}
	else if (target == LS_ADDR_NULL && backward)
	{
		// Walk backward until we find the label
		for (;;)
		{
			if (!_rewind_line(self))
				goto throw;

			// Quickly saving and restoring our location avoids
			// a costly second rewind to undo the ls_lex().
			ls_addr_t bw_pc = self->_pc++;
			ls_token_t tok = ls_lex(self);
			if (tok == LS_TOK_NUM_LABEL)
			{
				if (self->_token.number == num)
				{
					target = self->_pc;
					break;
				}
			}

			self->_pc = bw_pc;
		}
	}

	if (target == LS_ADDR_NULL)
	{
throw:
		self->_pc = pc;
		ls_throw_err(self, LS_UNDEFINED_LABEL);
	}
	else
	{
		self->_pc = target;
	}
}

void ls_goto_ident(ls_t * self, char const * ident)
{
	ls_value_t * label = NULL;

	for (ls_value_t * i = self->_labels; i; i = i->next)
	{
		if (!strncmp(i->body.label.ident, ident, LS_IDENT_LEN))
		{
			label = i;
			break;
		}
	}

	if (!label)
	{
		for (;;)
		{
			ls_token_t tok = ls_lex(self);

			if (tok == LS_TOK_STR_LABEL)
			{
				// New labels are always put at the head,
				// so test there. Don't just read the token
				// in case some processing was done.
				if (!strncmp(self->_labels->body.label.ident,
					ident, LS_IDENT_LEN))
				{
					label = self->_labels;
					break;
				}
			}
			else if (tok == LS_TOK_NONE)
			{
				break;
			}
		}
	}

	if (!label)
		ls_throw_err(self, LS_UNDEFINED_LABEL);

	self->_pc = label->body.label.pc;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static bool _rewind_line(ls_t * self)
{
	if (self->_pc == 0)
		return false;

	ls_addr_t pc = (ls_addr_t)(self->_pc - 1);

	for (;;)
	{
		unsigned char ch = ls_fetch(self, pc);

		if (ch == '\n')
		{
			self->_pc = pc;
			return true;
		}

		if (pc == 0)
			return false;
		--pc;
	}
}
