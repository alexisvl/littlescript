// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "ls.h"

// Supporting modules
#include "ls_kws.h"
#include "ls_expr.h"
#include "ls_internal.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

ls_error_t ls_run(ls_t * self, ls_value_t * pool, size_t szpool)
{
	if (setjmp(self->_error_jmp_buf))
	{
		return self->_error;
	}

	ls_init(self, pool, szpool);

	while (!self->stop)
	{
		if (!ls_exec_line(self))
			return LS_OK;
	}

	return LS_STOPPED;
}

#define TO_POOL_N(p) (p ? "" : "----"), \
	(p ? 4 : 0), (p ? (unsigned)(p - first) : 0)
#define POOL_F "%s%.*u"
void ls_print_value(FILE * stream, ls_value_t * value, ls_value_t * first)
{
	char buf[16];

	fprintf(stream, "<"POOL_F"> p<"POOL_F"> n<"POOL_F"> ",
		TO_POOL_N(value), TO_POOL_N(value->prev), TO_POOL_N(value->next)
	);

	switch (value->ty)
	{
	case LS_TY_NOT_ALLOC:
		fprintf(stream, "[free    ]");
		break;
	case LS_TY_PRISTINE:
		fprintf(stream, "[pristine]");
		break;
	case LS_TY_INT:
		fprintf(stream, "[int     ] %"PRId32, value->body.integer.value);
		break;
	case LS_TY_STR:
		memcpy(buf, value->body.str.value, 4);
		buf[4] = 0;
		fprintf(stream,
			"[str     ] len = %"PRIu16", "
			"chunk = <"POOL_F">, val = %s",
			value->body.str.length,
			TO_POOL_N(value->body.str.chunk),
			buf
		);
		break;
	case LS_TY_LIST:
		fprintf(stream,
			"[list    ] len = %"PRIu16", first = <"POOL_F">, "
			"memo = %"PRIu16" @ <"POOL_F">",
			value->body.list.length,
			TO_POOL_N(value->body.list.first),
			value->body.list.memo_i,
			TO_POOL_N(value->body.list.memo)
		);
		break;
	case LS_TY_STR_CHUNK:
		memcpy(buf, value->body.str_chunk.value, 8);
		buf[8] = 0;
		fprintf(stream, "[strchunk] %s", buf);
		break;
	case LS_TY_INT_VAR:
		memcpy(buf, value->body.int_var.ident, LS_IDENT_LEN);
		buf[LS_IDENT_LEN] = 0;
		fprintf(stream, "[intvar  ] %s = %"PRId32,
			buf,
			value->body.int_var.value
		);
		break;
	case LS_TY_VAR:
		memcpy(buf, value->body.var.ident, LS_IDENT_LEN);
		buf[LS_IDENT_LEN] = 0;
		fprintf(stream, "[var     ] %s = <"POOL_F">",
			buf,
			TO_POOL_N(value->body.var.value)
		);
		break;
	case LS_TY_LABEL:
		memcpy(buf, value->body.label.ident, LS_IDENT_LEN);
		buf[LS_IDENT_LEN] = 0;
		fprintf(stream, "[label   ] %s = %"PRIu16,
			buf,
			value->body.label.pc
		);
		break;
	case LS_TY_SCTX_CALL:
		fprintf(stream, "[sctxCALL] pc = %"PRIu16", readptr = %"PRIu16,
			value->body.sctx_call.pc,
			value->body.sctx_call.readptr
		);
		break;
	case LS_TY_SCTX_FOR:
		fprintf(stream, "[sctxFOR ] term = %"PRId32", step = %"PRId32,
			value->body.sctx_for.term,
			value->body.sctx_for.step
		);
		break;
	case LS_TY_SCTX_WHILE:
		fprintf(stream, "[sctxWHIL] pc = %"PRIu16,
			value->body.sctx_while.while_pc
		);
		break;
	default:
		fprintf(stream, "[inval   ] ty = %u", value->ty);
		break;
	}
}

bool ls_translate_pc(ls_t * self, ls_addr_t pc, uint16_t * line,
	uint16_t * col)
{
	uint16_t n_line = 1, n_col = 1;
	bool last_was_nl = false;

	for (ls_addr_t i = 0; i <= pc; i++)
	{
		int f = self->fetcher(self->fetcher_arg, i);

		if (f <= 0)
			return false;

		ls_uchar uch = (ls_uchar) f;

		if (last_was_nl)
		{
			n_line++;
			n_col = 1;
		}
		else
		{
			n_col++;
		}

		last_was_nl = (uch == (ls_uchar) '\n');
	}

	*line = n_line;
	*col = n_col;
	return true;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
