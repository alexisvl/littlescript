// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// This module
#include "ls_internal.h"

// Supporting modules
#include "ls_kws.h"
#include "ls_expr.h"
#include "ls_lex.h"

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <ctype.h>
#include <string.h>

// --- PRIVATE MACROS ----------------------------------------------------------

/// Check a return code and throw if it is an error.
#define CHECK_THROW(self, v) do {ls_error_t _e = (v); _throw_err((self), _e);} \
	while (0)

#define IS_WORD_CHAR(ch) (isalpha((ch)) || (ch) == '_' || (ch) >= 0x80)
#define IS_TOK_CHAR(ch) ((ch) >= 0x80)
#define IS_NUMBER_CHAR(ch) (isdigit((ch)) || (ch) == '&')
#define IS_WHITESPACE_CHAR(ch) ((ch) == ' ' || (ch) == '\t' || (ch) == 0x7F)
#define IS_SEPARATOR_CHAR(ch) ((ch) == '\n' || (ch) == '\r' || (ch) == ';')

// --- PRIVATE DATATYPES -------------------------------------------------------
// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------
// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------
// --- PUBLIC FUNCTIONS --------------------------------------------------------

void ls_init(ls_t * self, ls_value_t * pool, size_t szpool)
{
	self->_error = LS_OK;

	self->_pool = pool;

	if (pool)
	{
		self->_pool[szpool - 1].ty = LS_TY_PRISTINE;
		self->_pool[szpool - 1].next = NULL;
		self->_pool[szpool - 1].prev = NULL;

		for (size_t i = 0; i < szpool - 1; i++)
		{
			self->_pool[i].ty = LS_TY_PRISTINE;
			self->_pool[i].next = &self->_pool[i + 1];
			self->_pool[i].prev = NULL;
		}

		self->_callstack = self->_pool;
		self->_pool = self->_pool->next;
		*self->_callstack = (ls_value_t) {
			.ty = LS_TY_SCTX_CALL,
			.prev = NULL,
			.next = NULL,
			.body.sctx_call = {
				.pc = LS_ADDR_NULL,
				.readptr = LS_ADDR_NULL,
			},
		};
	}

	self->_labels = NULL;
	self->_pc = 0;
	self->stop = false;

	for (size_t i = 0; i < LS_LABEL_CACHE_SIZE; i++)
		self->_label_cache[i].pc = LS_ADDR_NULL;
	self->_label_cache_i = 0;
}

ls_kw_t ls_convert_kw(char const * word)
{
	uint8_t hash = LS_KW_HASH_DEFAULT;
	for (size_t i = 0; word[i]; i += 1)
	{
		char ch = word[i];
		hash = LS_KW_HASH(hash, ch);
	}
	hash = LS_KW_HASH_FINALIZE(hash);

	uint8_t i = ls_kw_hashmap_indices[hash];

	if (i == (uint8_t) -1)
		return LS_NOT_A_KW;

	do {
		uint8_t n_kw = LS_HASHMAP_ENTRY_UNPACK_SENTINEL(
			ls_kw_hashmap[i]
		);
		LS_PROGMEM const ls_kwdef_t * kwdef = &ls_kwmap[n_kw];

		// TODO PROGMEM
		if (!strcasecmp(kwdef->name, word))
			return (ls_kw_t) n_kw + LS_KW_OFFSET;
	} while (!LS_HASHMAP_ENTRY_IS_SENTINEL(ls_kw_hashmap[i++]));

	return LS_NOT_A_KW;
}

void ls_throw_err(ls_t * self, ls_error_t e)
{
	if (e)
	{
		self->_error = e;
		longjmp(self->_error_jmp_buf, 1);
	}
}

ls_uchar ls_fetch_rel(ls_t * self, int offset)
{
	if (self->_pc + offset > UINT16_MAX || self->_pc + offset < 0)
		ls_throw_err(self, LS_INTERNAL_ERROR);
	return ls_fetch(self, (ls_addr_t)(self->_pc + offset));
}

ls_uchar ls_fetch(ls_t * self, ls_addr_t pc)
{
	int rc = self->fetcher(self->fetcher_arg, pc);
	if (rc == -LS_NO_MORE_PROGRAM)
		return 0;
	else if (rc > 0)
		return (ls_uchar) rc;
	else if (rc <= -LS_ERROR_T_TOP)
		ls_throw_err(self, LS_INTERNAL_ERROR);
	else
		ls_throw_err(self, (ls_error_t) -rc);
	return 0;
}

ls_value_t * ls_alloc(ls_t * self)
{
	if (!self->_pool)
		ls_throw_err(self, LS_OUT_OF_MEMORY);

	ls_value_t * v = self->_pool;
	self->_pool = v->next;
	return v;
}

void ls_free(ls_t * self, ls_value_t * v)
{
	v->ty = LS_TY_NOT_ALLOC;
	v->next = self->_pool;
	self->_pool = v;
}

void ls_free_val(ls_t * self, ls_value_t * v)
{
	ls_value_t * child;

	switch (v->ty)
	{
	case LS_TY_STR:
		child = v->body.str.chunk;
		break;
	case LS_TY_LIST:
		child = v->body.list.first;
		break;
	case LS_TY_SCTX_CALL:
	case LS_TY_SCTX_FOR:
	case LS_TY_SCTX_WHILE:
		// Note that this is NOT normal behavior for any other types
		// and you should NEVER EVER use `child = v->next` if you don't
		// want to blow up the stack. Stack context types are unique
		// in using ->next as a janky member to point to their first
		// scope variable, simply because they were Quite Full Already
		// and didn't need a next pointer.
		child = v->next;
		break;
	case LS_TY_VAR:
		ls_free_val(self, v->body.var.value);
		child = NULL;
		break;
	default:
		child = NULL;
		break;
	}

	ls_value_t * next;
	for (; child; child = next)
	{
		next = child->next;
		ls_free_val(self, child);
	}

	ls_free(self, v);
}

size_t ls_mem_avail(ls_t * self)
{
	size_t count = 0;
	ls_value_t * iter = self->_pool;

	while (iter)
	{
		count++;
		iter = iter->next;
	}

	return count;
}

ls_value_t * ls_find_var(ls_t * self, char const * name)
{
	for (ls_value_t * scope = self->_callstack; scope; scope = scope->prev)
	{
		for (ls_value_t * var = scope->next; var; var = var->next)
		{
			char const * ident;
			if (var->ty == LS_TY_INT_VAR)
				ident = var->body.int_var.ident;
			else
				ident = var->body.var.ident;

			if (!strncmp(ident, name, LS_IDENT_LEN))
				return var;
		}
	}

	return NULL;
}

ls_value_t * ls_new_var(ls_t * self, char const * name)
{
	ls_value_t * scope = self->_callstack;

	while (scope && scope->ty != LS_TY_SCTX_CALL)
		scope = scope->prev;

	if (!scope)
		ls_throw_err(self, LS_INTERNAL_ERROR);

	ls_value_t * var = ls_alloc(self);
	var->ty = LS_TY_INT_VAR;
	memset(var->body.int_var.ident, 0, LS_IDENT_LEN);
	strncpy(var->body.int_var.ident, name, LS_IDENT_LEN);
	var->body.int_var.value = 0;

	var->next = scope->next;
	scope->next = var;

	return var;
}

ls_int_t ls_read_int_var(ls_t * self, ls_value_t * var)
{
	if (var->ty == LS_TY_INT_VAR)
		return var->body.int_var.value;
	else
		ls_throw_err(self, LS_TYPE_MISMATCH);

	// Unreachable
	return 0;
}

void ls_write_int_var(ls_t * self, ls_value_t * var, ls_int_t val)
{
	if (var->ty == LS_TY_VAR)
	{
		if (var->body.var.value)
			ls_free(self, var->body.var.value);
		var->ty = LS_TY_INT_VAR;
	}

	if (var->ty != LS_TY_INT_VAR)
		ls_throw_err(self, LS_INTERNAL_ERROR);

	var->body.int_var.value = val;
}

void ls_write_var(ls_t * self, ls_value_t * var, ls_value_t * val)
{
	if (val->ty == LS_TY_INT)
	{
		ls_write_int_var(self, var, val->body.integer.value);
	}
	else
	{
		if (var->ty == LS_TY_INT_VAR)
		{
			var->ty = LS_TY_VAR;
			var->body.var.value = NULL;
		}

		if (var->body.var.value)
			ls_free_val(self, var->body.var.value);

		var->body.var.value = ls_alloc(self);
		memcpy(var->body.var.value, val, sizeof(ls_value_t));
	}
}

bool ls_exec_line(ls_t * self)
{
	ls_token_t tok = LS_TOK_STATEMENT_SEP;

	while (tok == LS_TOK_STATEMENT_SEP)
		tok = ls_lex(self);

	if (self->line_trace_hook)
		self->line_trace_hook(self);

	if (tok == LS_TOK_STR_LABEL || tok == LS_TOK_NUM_LABEL)
		// These got cached by ls_lex() already, move on
		tok = ls_lex(self);

	// LET is handled here because of the slight complexity of implicit-LET
	if (tok == LS_KW_LET)
	{
		tok = ls_lex(self);
		if (tok != LS_TOK_WORD)
			ls_throw_err(self, LS_SYNTAX_ERROR);
	}

	// Now both implicit and explicit LET can be handled
	if (tok == LS_TOK_WORD)
	{
		ls_value_t * var = ls_find_var(self, self->_token.word);

		if (!var)
			var = ls_new_var(self, self->_token.word);

		if (ls_lex(self) != LS_OP_EQ)
			ls_throw_err(self, LS_SYNTAX_ERROR);

		ls_value_t val;
		ls_eval_expr(self, &val, LS_TOK_NONE);

		if (val.ty != LS_TY_INT)
			ls_throw_err(self, LS_TYPE_MISMATCH);

		ls_write_int_var(self, var, val.body.integer.value);
	}
	else if (LS_TOK_KEYWORD(tok))
	{
		ls_kwmap[tok - LS_KW_OFFSET].fun(self);
	}
	else if (tok == LS_TOK_STATEMENT_SEP)
	{
		return true;
	}
	else if (tok == LS_TOK_NONE)
	{
		return false;
	}
	else
	{
		ls_throw_err(self, LS_SYNTAX_ERROR);
	}

	return true;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------
