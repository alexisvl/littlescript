// This software disclaims copyright. Do what you want with it. Be gay, do
// crime. Originally written by Alexis Lockwood in 2021. Ⓐ

// --- DEPENDENCIES ------------------------------------------------------------

// Supporting modules
#include "ls.h"
#include "ls_internal.h"
#include "ls_lex.h"

// External dependencies
#include <libexplain/ferror.h>
#include <libexplain/fopen.h>
#include <libexplain/malloc.h>
#include <libexplain/realloc.h>

// Standard headers
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <limits.h>
#include <string.h>

// --- PRIVATE MACROS ----------------------------------------------------------
// --- PRIVATE DATATYPES -------------------------------------------------------

typedef struct {
	char const * s;
	size_t len;
} file_fetcher_t;

typedef struct {
	ls_t ls;
	ls_token_t last_tok;
	file_fetcher_t * fet;
	bool add_space;
	FILE * f_out;
} minifier_t;

// --- PRIVATE CONSTANTS -------------------------------------------------------
// --- PRIVATE FUNCTION PROTOTYPES ---------------------------------------------

static int _fetcher(void * arg, uint16_t loc);
static void _usage(char const * argv0, bool short_text);
static void _minify(file_fetcher_t * fet, FILE * f_out);

static void _min_number(minifier_t * min, ls_token_t tok);
static void _min_word(minifier_t * min, ls_token_t tok);
static void _min_str_label(minifier_t * min, ls_token_t tok);
static void _min_num_label(minifier_t * min, ls_token_t tok);
static ls_token_t _min_keyword(minifier_t * min, ls_token_t tok);
static void _min_string(minifier_t * min, ls_token_t tok);
static void _min_operator(minifier_t * min, ls_token_t tok);
static void _min_comma(minifier_t * min, ls_token_t tok);
static void _min_sep(minifier_t * min, ls_token_t tok);

// --- PUBLIC VARIABLES --------------------------------------------------------
// --- PRIVATE VARIABLES -------------------------------------------------------

static bool keep_rems;
static bool un_minify;

// --- PUBLIC FUNCTIONS --------------------------------------------------------

int main(int argc, char ** argv)
{
	int opt;
	while ((opt = getopt(argc, argv, "hru")) != -1)
	{
		switch (opt)
		{
		default:
			_usage(argv[0], false);
			exit(opt == 'h' ? EXIT_SUCCESS : EXIT_FAILURE);
			break;

		case 'r':
			keep_rems = true;
			break;

		case 'u':
			un_minify = true;
			break;
		}
	}

	FILE * f = NULL;

	if (optind >= argc)
		f = stdin;
	else if (!strcmp(argv[optind], "-"))
		f = stdin;
	else
		f = explain_fopen_or_die(argv[optind], "r");

	size_t sz = 512, i = 0;
	char * s = explain_malloc_or_die(sz);

	while (!feof(f))
	{
		explain_ferror_or_die(f);

		size_t read_sz = sz - i;
		size_t n = fread(&s[i], 1, sz - i, f);
		i += n;

		if (read_sz == n)
		{
			sz *= 2;
			s = explain_realloc_or_die(s, sz);
		}
	}

	if (f != stdin)
		fclose(f);

	file_fetcher_t fet;
	fet.len = i;
	fet.s = s;

	if (optind + 1 < argc)
	{
		if (!strcmp(argv[optind + 1], "-"))
			f = stdout;
		else
			f = explain_fopen_or_die(argv[optind + 1], "w");
	}
	else
		f = stdout;

	_minify(&fet, f);
	free(s);

	if (f != stdout)
		fclose(f);

	return 0;
}

// --- PRIVATE FUNCTION DEFINITIONS --------------------------------------------

static int _fetcher(void * arg, uint16_t loc)
{
	file_fetcher_t * fet = (file_fetcher_t *) arg;

	if ((size_t) loc >= fet->len)
		return -LS_NO_MORE_PROGRAM;
	else
		return (ls_uchar) fet->s[(size_t) loc];
}

static void _usage(char const * argv0, bool short_text)
{
	fprintf(stderr, "usage: %s [OPTION...] [INPUT [OUTPUT]]\n", argv0);

	if (short_text)
		return;

	fprintf(stderr, "Minify a Littlescript file\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "  -h    display this help text\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "  -r    keep REM comments\n");
	fprintf(stderr, "  -u    un-minify\n");
}

static void _minify(file_fetcher_t * fet, FILE * f_out)
{
	minifier_t min = {.fet = fet};
	ls_value_t pool[100]; // for labels

	ls_init(&min.ls, pool, sizeof pool / sizeof pool[0]);
	min.ls.fetcher = _fetcher;
	min.ls.fetcher_arg = (void *) fet;

	if (setjmp(min.ls._error_jmp_buf))
	{
		uint16_t line = 0, col = 0;
		ls_translate_pc(&min.ls, min.ls._pc, &line, &col);
		fprintf(stderr, "error %d at %u:%u",
			(int) min.ls._error, line, col);
		exit(EXIT_FAILURE);
	}

	min.f_out = f_out;
	min.last_tok = LS_TOK_NONE;

	// TODO: actually collect idents and label numbers, and minify them
	// too
	bool done = false;
	while (!done)
	{
		ls_token_t tok = ls_lex(&min.ls);

		min.add_space =
			min.last_tok == LS_TOK_NUMBER ||
			min.last_tok == LS_TOK_WORD ||
			(LS_TOK_KEYWORD(min.last_tok) && un_minify) ||
			(LS_TOK_OPER(min.last_tok) && un_minify) ||
			(min.last_tok == LS_TOK_COMMA && un_minify);

		ls_token_t tok_for_last_tok = tok;

		switch (tok)
		{
		case LS_TOK_NUMBER:
			_min_number(&min, tok);
			break;

		case LS_TOK_WORD:
			_min_word(&min, tok);
			break;

		case LS_TOK_STR_LABEL:
			_min_str_label(&min, tok);
			break;

		case LS_TOK_NUM_LABEL:
			_min_num_label(&min, tok);
			break;

		case LS_TOK_STRING:
			_min_string(&min, tok);
			break;

		case LS_TOK_COMMA:
			_min_comma(&min, tok);
			break;

		case LS_TOK_STATEMENT_SEP:
			_min_sep(&min, tok);
			break;

		case LS_TOK_INVALID:
			ls_throw_err(&min.ls, LS_SYNTAX_ERROR);
			break;

		default:
			if (LS_TOK_KEYWORD(tok))
				tok_for_last_tok = _min_keyword(&min, tok);
			else if (LS_TOK_OPER(tok))
				_min_operator(&min, tok);
			else
				ls_throw_err(&min.ls, LS_INTERNAL_ERROR);
			break;

		case LS_TOK_NONE:
			done = true;
			break;
		}

		min.last_tok = tok_for_last_tok;
	}
}

static void _min_number(minifier_t * min, ls_token_t tok)
{
	(void) tok;
	if (min->add_space)
		fprintf(min->f_out, " ");
	fprintf(min->f_out, "%"PRId32, min->ls._token.number);
}

static void _min_word(minifier_t * min, ls_token_t tok)
{
	(void) tok;
	if (min->add_space)
		fprintf(min->f_out, " ");
	assert(min->ls._token.word[LS_IDENT_OR_KW_LEN] == 0);
	min->ls._token.word[LS_IDENT_OR_KW_LEN] = 0;
	fprintf(min->f_out, "%s", min->ls._token.word);
}

static void _min_str_label(minifier_t * min, ls_token_t tok)
{
	(void) tok;
	if (min->add_space)
		fprintf(min->f_out, " ");
	assert(min->ls._token.word[LS_IDENT_OR_KW_LEN] == 0);
	min->ls._token.word[LS_IDENT_OR_KW_LEN] = 0;
	fprintf(min->f_out, "%s:%s", min->ls._token.word,
		un_minify ? " " : "");
}

static void _min_num_label(minifier_t * min, ls_token_t tok)
{
	(void) tok;
	if (min->add_space)
		fprintf(min->f_out, " ");
	fprintf(min->f_out, "%"PRId32":%s", min->ls._token.number,
		un_minify ? " " : "");
}

static ls_token_t _min_keyword(minifier_t * min, ls_token_t tok)
{
	if (tok == LS_KW_REM)
	{
		if (un_minify)
		{
			if (min->add_space)
				fprintf(min->f_out, " ");
			fprintf(min->f_out, "REM ");
		}
		else if (keep_rems)
		{
			uint8_t c = LS_KW_REM;
			fwrite(&c, 1, 1, min->f_out);
		}

		ls_addr_t pc_start;
		ls_addr_t pc_end;
		for (pc_start = min->ls._pc;; pc_start++)
		{
			if (pc_start >= min->fet->len)
				break;
			if (!(min->fet->s[pc_start] == ' '
				|| min->fet->s[pc_start] == '\t'))
				break;
		}
		for (pc_end = pc_start;; pc_end++)
		{
			if (pc_end >= min->fet->len)
				break;
			if (min->fet->s[pc_end] == '\n')
				break;
		}

		min->ls._pc = (ls_addr_t)(pc_end + 1);
		if (un_minify || keep_rems)
		{
			fwrite(&min->fet->s[pc_start],
				(size_t)(pc_end - pc_start), 1, min->f_out);

			fprintf(min->f_out, "\n");

			// We just emitted a \n, make sure the
			// next statement reflects that
			return LS_TOK_STATEMENT_SEP;
		}
		else
		{
			// Skipped entirely...
			return (uint8_t) min->last_tok;
		}
	}
	else if (!un_minify)
	{
		uint8_t c = (uint8_t) tok;
		fwrite(&c, 1, 1, min->f_out);
		return tok;
	}
	else
	{
		if (min->add_space)
			fprintf(min->f_out, " ");
		fprintf(min->f_out, "%s",
			ls_kwmap[tok - LS_KW_OFFSET].name);
		return tok;
	}
}

static void _min_string(minifier_t * min, ls_token_t tok)
{
	(void) tok;
	if (un_minify)
		fprintf(min->f_out, " ");

	fprintf(min->f_out, "\"");

	for (ls_addr_t i = min->ls._token.string[0];
		i <= min->ls._token.string[1];
		i++)
	{
		fprintf(min->f_out, "%c", min->fet->s[i]);
	}

	fprintf(min->f_out, "\"");
}

static void _min_operator(minifier_t * min, ls_token_t tok)
{
	// TODO: this could be centralized
	static const unsigned char ops[][3] = {
		[LS_OP_LEQ] = "<=",
		[LS_OP_GEQ] = ">=",
		[LS_OP_NEQ] = "<>",
		[LS_OP_LPAREN] = "(",
		[LS_OP_RPAREN] = ")",
		[LS_OP_MOD] = "%",
		[LS_OP_MUL] = "*",
		[LS_OP_ADD] = "+",
		[LS_OP_SUB] = "-",
		[LS_OP_DIV] = "/",
		[LS_OP_POW] = "^",
		[LS_OP_LT] = "<",
		[LS_OP_EQ] = "=",
		[LS_OP_GT] = ">",
	};

	assert(tok <= LS_OP_GT);

	if (un_minify)
		fprintf(min->f_out, " ");

	fprintf(min->f_out, "%s", ops[tok]);
}

static void _min_comma(minifier_t * min, ls_token_t tok)
{
	(void) tok;
	fprintf(min->f_out, ",");
}

static void _min_sep(minifier_t * min, ls_token_t tok)
{
	(void) tok;

	if (min->last_tok != LS_TOK_STATEMENT_SEP &&
		min->last_tok != LS_TOK_STR_LABEL &&
		min->last_tok != LS_TOK_NUM_LABEL
	)
		fprintf(min->f_out, "\n");
}
