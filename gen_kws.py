#!/usr/bin/python3
# This software disclaims copyright. Do what you want with it. Be gay, do
# crime. Originally written by Alexis Lockwood in 2021. Ⓐ

"""Generates keyword files for LittleScript.

Usage: gen_kws.py source
       gen_kws.py header
       gen_kws.py show
"""

import sys

OFFSET = 0x80
MAX_HASH = 0x1F
HM_SENTINEL = 0x80

class Kw:
    def __init__(self, name, index):
        self.h = sum(ord(i) for i in name) & 0x1F
        self.name = name
        self.index = index

# These should be contiguous and may span from 0x00 to 0x7F. While this list
# is currently in alphabetical order, it doesn't need to be, and indices should
# be fixed once assigned - add new keywords at the bottom.
KEYWORDS = [
    Kw("ABS",   0x80),
    Kw("AND",   0x81),
    Kw("AS",    0x82),
    Kw("ASC",   0x83),
    Kw("AT",    0x84),
    Kw("ATN",   0x85),
    Kw("CALL",  0x86),
    Kw("CAT",   0x87),
    Kw("CHR",   0x88),
    Kw("CLOSE", 0x89),
    Kw("COS",   0x8A),
    Kw("COUNT", 0x8B),
    Kw("DATA",  0x8C),
    Kw("DEC",   0x8D),
    Kw("DEF",   0x8E),
    Kw("END",   0x8F),
    Kw("EQV",   0x90),
    Kw("ERASE", 0x91),
    Kw("ERROR", 0x92),
    Kw("FN",    0x93),
    Kw("FOR",   0x94),
    Kw("GOSUB", 0x95),
    Kw("GOTO",  0x96),
    Kw("HEX",   0x97),
    Kw("IF",    0x98),
    Kw("IMP",   0x99),
    Kw("INPUT", 0x9A),
    Kw("LEFT",  0x9B),
    Kw("LET",   0x9C),
    Kw("LOG",   0x9D),
    Kw("MID",   0x9E),
    Kw("NEXT",  0x9F),
    Kw("NOT",   0xA0),
    Kw("OCT",   0xA1),
    Kw("ON",    0xA2),
    Kw("OR",    0xA3),
    Kw("OPEN",  0xA4),
    Kw("PACK",  0xA5),
    Kw("PRINT", 0xA6),
    Kw("RANDOMIZE", 0xA7),
    Kw("READ",  0xA8),
    Kw("REM",   0xA9),
    Kw("RESTORE", 0xAA),
    Kw("RETURN", 0xAB),
    Kw("RIGHT", 0xAC),
    Kw("RND",   0xAD),
    Kw("SIN",   0xAE),
    Kw("SQR",   0xAF),
    Kw("STEP",  0xB0),
    Kw("SWAP",  0xB1),
    Kw("TAN",   0xB2),
    Kw("THEN",  0xB3),
    Kw("TO",    0xB4),
    Kw("UNPACK", 0xB5),
    Kw("UNTIL", 0xB6),
    Kw("VAL",   0xB7),
    Kw("WEND",  0xB8),
    Kw("WHILE", 0xB9),
    Kw("WRITE", 0xBA),
    Kw("XOR", 0xBB),
]

if len(sys.argv) == 2 and sys.argv[1] == "source":
    TARGET = "source"
elif len(sys.argv) == 2 and sys.argv[1] == "header":
    TARGET = "header"
elif len(sys.argv) == 2 and sys.argv[1] == "show":
    TARGET = "show"
else:
    print(__doc__)
    sys.exit()

hashmap = []
indices = []

for h in range(MAX_HASH):
    kws = [i.index - OFFSET for i in KEYWORDS if i.h == h]

    if kws:
        #kws[-1] |= HM_SENTINEL
        kws[-1] = f"0x80 | {kws[-1]}" # Easier to read
        indices.append(len(hashmap))
        hashmap.extend(kws)
    else:
        indices.append("LS_HASHMAP_NULL_VALUE")

if TARGET == "show":
    for i in KEYWORDS:
        print(f"{i.name:<12}: hash = {i.h:2}, same hash: ", end='')
        for j in KEYWORDS:
            if j.h == i.h:
                print(j.name, end=' ')
        print()
    sys.exit()

print("// This code was auto-generated")

if TARGET == "source":
    print('#include "ls_kws.h"')
    print('#include "ls_internal.h"')
    print()

    print('const LS_PROGMEM uint8_t ls_kw_hashmap_indices[] = {')
    for n, i in enumerate(indices):
        print(f'    /* {n} */ {i},')
    print('};')
    print()
    print('const LS_PROGMEM uint8_t ls_kw_hashmap[] = {')
    for n, i in enumerate(hashmap):
        print(f'    /* {n} */ {i},')
    print('};')
    print()

    for i in KEYWORDS:
        print(f'LS_KW_FUN({i.name});')
        print(f'static const LS_PROGMEM char _kw_name_{i.name}[] = '
              f'"{i.name}";')

    print()
    print('const LS_PROGMEM ls_kwdef_t ls_kwmap[] = {')
    for n, i in enumerate(KEYWORDS):
        print(f'    /* {n} */ {{_kw_name_{i.name}, '
              f'ls_kw_fun_{i.name} }},')
    print('};')

    sys.exit()

if TARGET == "header":
    print('#ifndef LS_KWS_H')
    print('#define LS_KWS_H')
    print('typedef enum {')
    top = max(i.index for i in KEYWORDS)
    for i in range(OFFSET, top + 1):
        match = [j for j in KEYWORDS if j.index == i]
        assert len(match) == 1
        assert match[0].index == i
        print(f'    LS_KW_{match[0].name} = 0x{i:02X},')
    print('    LS_MAX_KWS')
    print('} ls_kw_t;')

    print('#define LS_NOT_A_KW LS_MAX_KWS')
    print(f'#define LS_KW_OFFSET 0x{OFFSET:X}')
    print('#endif // !defined(LS_INTERNAL_H)')
