REM examples/add.bas
a = 2
b = 3
GOSUB Add(a = a, b = b) AS c
PRINT "a = ", a, ", b = ", b
PRINT "a + b = ", c
END

Add:	'Add(a, b) => a + b
	' try to tinker with the scope to make sure it's set up right
	d = a
	a = 42
	RETURN d + b
